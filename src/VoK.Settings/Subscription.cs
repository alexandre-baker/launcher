﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoK
{
    public class Subscription
    {
        [JsonProperty("game")]
        public GameId Game { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("defaultServer")]
        public string DefaultServer { get; set; }

        [JsonProperty("shortcuts")]
        public List<Shortcut> Shortcuts { get; set; } = new List<Shortcut>();
    }
}
