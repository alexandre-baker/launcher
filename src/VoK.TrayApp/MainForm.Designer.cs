﻿namespace VoK.TrayApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.scHeader = new System.Windows.Forms.SplitContainer();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.scNav = new System.Windows.Forms.SplitContainer();
            this.btnAccounts = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.scMain = new System.Windows.Forms.SplitContainer();
            this.pSettings = new System.Windows.Forms.Panel();
            this.btnPatch = new System.Windows.Forms.Button();
            this.pHome = new System.Windows.Forms.Panel();
            this.btnWikiPage = new System.Windows.Forms.Button();
            this.btnGitlab = new System.Windows.Forms.Button();
            this.btnDiscord = new System.Windows.Forms.Button();
            this.btnWeb = new System.Windows.Forms.Button();
            this.pAccounts = new System.Windows.Forms.Panel();
            this.lblRuntimeInfo = new System.Windows.Forms.Label();
            this.pUserSettings = new System.Windows.Forms.Panel();
            this.cbMinimizeOnLaunch = new System.Windows.Forms.CheckBox();
            this.cbEnablePatchNotifications = new System.Windows.Forms.CheckBox();
            this.btnLOTROPatchNow = new System.Windows.Forms.Button();
            this.btnDDOPatchNow = new System.Windows.Forms.Button();
            this.cbAutoPatch = new System.Windows.Forms.CheckBox();
            this.btnFindLotro = new System.Windows.Forms.Button();
            this.btnFindDdo = new System.Windows.Forms.Button();
            this.lblLotroPath = new System.Windows.Forms.Label();
            this.btnUserSettingsSave = new System.Windows.Forms.Button();
            this.cbEnableLogging = new System.Windows.Forms.CheckBox();
            this.cbKeepRawData = new System.Windows.Forms.CheckBox();
            this.txtBxLOTROPath = new System.Windows.Forms.TextBox();
            this.txtBxDDOPath = new System.Windows.Forms.TextBox();
            this.lblDdoPath = new System.Windows.Forms.Label();
            this.pEditAccount = new System.Windows.Forms.Panel();
            this.btnRemoveAccount = new System.Windows.Forms.Button();
            this.tbEditDisplayName = new System.Windows.Forms.TextBox();
            this.lEditDisplay = new System.Windows.Forms.Label();
            this.btnEditAccountCancel = new System.Windows.Forms.Button();
            this.btnEditAccountSave = new System.Windows.Forms.Button();
            this.tbEditPassword = new System.Windows.Forms.TextBox();
            this.lEditPassword = new System.Windows.Forms.Label();
            this.tbEditUsername = new System.Windows.Forms.TextBox();
            this.lEditUsername = new System.Windows.Forms.Label();
            this.scAccountDetails = new System.Windows.Forms.SplitContainer();
            this.tbShortcutDisplayName = new System.Windows.Forms.TextBox();
            this.btnCancelShortcut = new System.Windows.Forms.Button();
            this.btnAddShortcut = new System.Windows.Forms.Button();
            this.tbAddShortcutCharacter = new System.Windows.Forms.TextBox();
            this.cbMakeDefault = new System.Windows.Forms.CheckBox();
            this.btnPlay = new System.Windows.Forms.Button();
            this.cbPlayServers = new System.Windows.Forms.ComboBox();
            this.scAccountDetailsLower = new System.Windows.Forms.SplitContainer();
            this.pShortcuts = new System.Windows.Forms.Panel();
            this.lblShortcuts = new System.Windows.Forms.Label();
            this.btnEditSubSave = new System.Windows.Forms.Button();
            this.tbEditSubDisplayName = new System.Windows.Forms.TextBox();
            this.lblEditSubDisplayName = new System.Windows.Forms.Label();
            this.lblSubscriptionInfo = new System.Windows.Forms.Label();
            this.pMessages = new System.Windows.Forms.Panel();
            this.btnMessaagesOkay = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.pAddAccount = new System.Windows.Forms.Panel();
            this.tbAccountDisplayName = new System.Windows.Forms.TextBox();
            this.lDisplayName = new System.Windows.Forms.Label();
            this.btnAccountCancel = new System.Windows.Forms.Button();
            this.btnAccountSave = new System.Windows.Forms.Button();
            this.tbAccountPassword = new System.Windows.Forms.TextBox();
            this.lPassword = new System.Windows.Forms.Label();
            this.tbAccountUsername = new System.Windows.Forms.TextBox();
            this.lUsername = new System.Windows.Forms.Label();
            this.pMainMenu = new System.Windows.Forms.Panel();
            this.btnRefreshStatus = new System.Windows.Forms.Button();
            this.lblServerStatus = new System.Windows.Forms.Label();
            this.ilMain = new System.Windows.Forms.ImageList(this.components);
            this.cmsDDO = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsLotro = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cb64bit = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.scHeader)).BeginInit();
            this.scHeader.Panel1.SuspendLayout();
            this.scHeader.Panel2.SuspendLayout();
            this.scHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scNav)).BeginInit();
            this.scNav.Panel1.SuspendLayout();
            this.scNav.Panel2.SuspendLayout();
            this.scNav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).BeginInit();
            this.scMain.Panel1.SuspendLayout();
            this.scMain.Panel2.SuspendLayout();
            this.scMain.SuspendLayout();
            this.pSettings.SuspendLayout();
            this.pHome.SuspendLayout();
            this.pAccounts.SuspendLayout();
            this.pUserSettings.SuspendLayout();
            this.pEditAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAccountDetails)).BeginInit();
            this.scAccountDetails.Panel1.SuspendLayout();
            this.scAccountDetails.Panel2.SuspendLayout();
            this.scAccountDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAccountDetailsLower)).BeginInit();
            this.scAccountDetailsLower.Panel1.SuspendLayout();
            this.scAccountDetailsLower.Panel2.SuspendLayout();
            this.scAccountDetailsLower.SuspendLayout();
            this.pShortcuts.SuspendLayout();
            this.pMessages.SuspendLayout();
            this.pAddAccount.SuspendLayout();
            this.pMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // scHeader
            // 
            this.scHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.scHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scHeader.IsSplitterFixed = true;
            this.scHeader.Location = new System.Drawing.Point(0, 0);
            this.scHeader.Margin = new System.Windows.Forms.Padding(1);
            this.scHeader.Name = "scHeader";
            this.scHeader.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scHeader.Panel1
            // 
            this.scHeader.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.scHeader.Panel1.Controls.Add(this.btnMinimize);
            this.scHeader.Panel1.Controls.Add(this.btnClose);
            this.scHeader.Panel1.Controls.Add(this.lblTitle);
            this.scHeader.Panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.scHeader_Panel1_MouseDown);
            this.scHeader.Panel1MinSize = 20;
            // 
            // scHeader.Panel2
            // 
            this.scHeader.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.scHeader.Panel2.Controls.Add(this.scNav);
            this.scHeader.Size = new System.Drawing.Size(1104, 628);
            this.scHeader.SplitterDistance = 33;
            this.scHeader.SplitterWidth = 1;
            this.scHeader.TabIndex = 0;
            // 
            // btnMinimize
            // 
            this.btnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.Location = new System.Drawing.Point(1054, 5);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(18, 19);
            this.btnMinimize.TabIndex = 3;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1078, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(18, 19);
            this.btnClose.TabIndex = 1;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.lblTitle.Location = new System.Drawing.Point(2, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(196, 18);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "THE VAULT OF KUNDARAK";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitle_MouseDown);
            // 
            // scNav
            // 
            this.scNav.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(79)))), ((int)(((byte)(153)))));
            this.scNav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scNav.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scNav.IsSplitterFixed = true;
            this.scNav.Location = new System.Drawing.Point(0, 0);
            this.scNav.Margin = new System.Windows.Forms.Padding(0);
            this.scNav.Name = "scNav";
            // 
            // scNav.Panel1
            // 
            this.scNav.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.scNav.Panel1.Controls.Add(this.btnAccounts);
            this.scNav.Panel1.Controls.Add(this.btnSettings);
            this.scNav.Panel1.Controls.Add(this.btnHome);
            this.scNav.Panel1MinSize = 46;
            // 
            // scNav.Panel2
            // 
            this.scNav.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(79)))), ((int)(((byte)(153)))));
            this.scNav.Panel2.Controls.Add(this.scMain);
            this.scNav.Size = new System.Drawing.Size(1102, 592);
            this.scNav.SplitterDistance = 46;
            this.scNav.SplitterWidth = 1;
            this.scNav.TabIndex = 0;
            // 
            // btnAccounts
            // 
            this.btnAccounts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccounts.FlatAppearance.BorderSize = 0;
            this.btnAccounts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccounts.Image = ((System.Drawing.Image)(resources.GetObject("btnAccounts.Image")));
            this.btnAccounts.Location = new System.Drawing.Point(0, 0);
            this.btnAccounts.Margin = new System.Windows.Forms.Padding(0);
            this.btnAccounts.Name = "btnAccounts";
            this.btnAccounts.Size = new System.Drawing.Size(46, 50);
            this.btnAccounts.TabIndex = 2;
            this.btnAccounts.UseVisualStyleBackColor = false;
            this.btnAccounts.Click += new System.EventHandler(this.btnAccounts_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSettings.Image")));
            this.btnSettings.Location = new System.Drawing.Point(0, 100);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(0);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(46, 50);
            this.btnSettings.TabIndex = 1;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnHome
            // 
            this.btnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.Location = new System.Drawing.Point(0, 50);
            this.btnHome.Margin = new System.Windows.Forms.Padding(0);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(46, 50);
            this.btnHome.TabIndex = 0;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // scMain
            // 
            this.scMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(79)))), ((int)(((byte)(153)))));
            this.scMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scMain.IsSplitterFixed = true;
            this.scMain.Location = new System.Drawing.Point(0, 0);
            this.scMain.Name = "scMain";
            // 
            // scMain.Panel1
            // 
            this.scMain.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(79)))), ((int)(((byte)(153)))));
            this.scMain.Panel1.Controls.Add(this.pSettings);
            this.scMain.Panel1.Controls.Add(this.pHome);
            this.scMain.Panel1.Controls.Add(this.pAccounts);
            // 
            // scMain.Panel2
            // 
            this.scMain.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.scMain.Panel2.Controls.Add(this.pUserSettings);
            this.scMain.Panel2.Controls.Add(this.pEditAccount);
            this.scMain.Panel2.Controls.Add(this.scAccountDetails);
            this.scMain.Panel2.Controls.Add(this.pMessages);
            this.scMain.Panel2.Controls.Add(this.pAddAccount);
            this.scMain.Panel2.Controls.Add(this.pMainMenu);
            this.scMain.Size = new System.Drawing.Size(1055, 592);
            this.scMain.SplitterDistance = 240;
            this.scMain.SplitterWidth = 1;
            this.scMain.TabIndex = 0;
            // 
            // pSettings
            // 
            this.pSettings.Controls.Add(this.btnPatch);
            this.pSettings.Location = new System.Drawing.Point(0, 366);
            this.pSettings.Margin = new System.Windows.Forms.Padding(0);
            this.pSettings.Name = "pSettings";
            this.pSettings.Size = new System.Drawing.Size(224, 137);
            this.pSettings.TabIndex = 6;
            // 
            // btnPatch
            // 
            this.btnPatch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPatch.FlatAppearance.BorderSize = 0;
            this.btnPatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPatch.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPatch.ForeColor = System.Drawing.Color.White;
            this.btnPatch.Image = global::VoK.TrayApp.Properties.Resources.settings_altered;
            this.btnPatch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPatch.Location = new System.Drawing.Point(0, 0);
            this.btnPatch.Margin = new System.Windows.Forms.Padding(0);
            this.btnPatch.Name = "btnPatch";
            this.btnPatch.Size = new System.Drawing.Size(224, 50);
            this.btnPatch.TabIndex = 3;
            this.btnPatch.Text = "Settings";
            this.btnPatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPatch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPatch.UseVisualStyleBackColor = false;
            // 
            // pHome
            // 
            this.pHome.Controls.Add(this.btnWikiPage);
            this.pHome.Controls.Add(this.btnGitlab);
            this.pHome.Controls.Add(this.btnDiscord);
            this.pHome.Controls.Add(this.btnWeb);
            this.pHome.Location = new System.Drawing.Point(0, 0);
            this.pHome.Margin = new System.Windows.Forms.Padding(0);
            this.pHome.Name = "pHome";
            this.pHome.Size = new System.Drawing.Size(262, 269);
            this.pHome.TabIndex = 5;
            // 
            // btnWikiPage
            // 
            this.btnWikiPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWikiPage.FlatAppearance.BorderSize = 0;
            this.btnWikiPage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWikiPage.Font = new System.Drawing.Font("Neo Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWikiPage.ForeColor = System.Drawing.Color.White;
            this.btnWikiPage.Image = ((System.Drawing.Image)(resources.GetObject("btnWikiPage.Image")));
            this.btnWikiPage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWikiPage.Location = new System.Drawing.Point(0, 150);
            this.btnWikiPage.Margin = new System.Windows.Forms.Padding(0);
            this.btnWikiPage.Name = "btnWikiPage";
            this.btnWikiPage.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.btnWikiPage.Size = new System.Drawing.Size(224, 50);
            this.btnWikiPage.TabIndex = 4;
            this.btnWikiPage.Text = " Wiki Page";
            this.btnWikiPage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWikiPage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnWikiPage.UseVisualStyleBackColor = false;
            this.btnWikiPage.Click += new System.EventHandler(this.btnWikiPage_Click);
            // 
            // btnGitlab
            // 
            this.btnGitlab.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGitlab.FlatAppearance.BorderSize = 0;
            this.btnGitlab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGitlab.Font = new System.Drawing.Font("Neo Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGitlab.ForeColor = System.Drawing.Color.White;
            this.btnGitlab.Image = ((System.Drawing.Image)(resources.GetObject("btnGitlab.Image")));
            this.btnGitlab.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGitlab.Location = new System.Drawing.Point(0, 100);
            this.btnGitlab.Margin = new System.Windows.Forms.Padding(0);
            this.btnGitlab.Name = "btnGitlab";
            this.btnGitlab.Size = new System.Drawing.Size(224, 50);
            this.btnGitlab.TabIndex = 3;
            this.btnGitlab.Text = "Gitlab";
            this.btnGitlab.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGitlab.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGitlab.UseVisualStyleBackColor = false;
            this.btnGitlab.Click += new System.EventHandler(this.btnGitlab_Click);
            // 
            // btnDiscord
            // 
            this.btnDiscord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDiscord.FlatAppearance.BorderSize = 0;
            this.btnDiscord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiscord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscord.ForeColor = System.Drawing.Color.White;
            this.btnDiscord.Image = ((System.Drawing.Image)(resources.GetObject("btnDiscord.Image")));
            this.btnDiscord.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDiscord.Location = new System.Drawing.Point(-3, 50);
            this.btnDiscord.Margin = new System.Windows.Forms.Padding(0);
            this.btnDiscord.Name = "btnDiscord";
            this.btnDiscord.Size = new System.Drawing.Size(224, 50);
            this.btnDiscord.TabIndex = 2;
            this.btnDiscord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDiscord.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDiscord.UseVisualStyleBackColor = false;
            this.btnDiscord.Click += new System.EventHandler(this.btnDiscord_Click);
            // 
            // btnWeb
            // 
            this.btnWeb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWeb.FlatAppearance.BorderSize = 0;
            this.btnWeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWeb.Font = new System.Drawing.Font("Neo Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWeb.ForeColor = System.Drawing.Color.White;
            this.btnWeb.Image = ((System.Drawing.Image)(resources.GetObject("btnWeb.Image")));
            this.btnWeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWeb.Location = new System.Drawing.Point(0, 0);
            this.btnWeb.Margin = new System.Windows.Forms.Padding(0);
            this.btnWeb.Name = "btnWeb";
            this.btnWeb.Size = new System.Drawing.Size(224, 50);
            this.btnWeb.TabIndex = 1;
            this.btnWeb.Text = " Website";
            this.btnWeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnWeb.UseVisualStyleBackColor = false;
            this.btnWeb.Click += new System.EventHandler(this.btnWeb_Click);
            // 
            // pAccounts
            // 
            this.pAccounts.AutoScroll = true;
            this.pAccounts.AutoSize = true;
            this.pAccounts.Controls.Add(this.lblRuntimeInfo);
            this.pAccounts.Location = new System.Drawing.Point(3, 174);
            this.pAccounts.Margin = new System.Windows.Forms.Padding(0);
            this.pAccounts.Name = "pAccounts";
            this.pAccounts.Size = new System.Drawing.Size(299, 192);
            this.pAccounts.TabIndex = 4;
            // 
            // lblRuntimeInfo
            // 
            this.lblRuntimeInfo.AutoSize = true;
            this.lblRuntimeInfo.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRuntimeInfo.ForeColor = System.Drawing.Color.White;
            this.lblRuntimeInfo.Location = new System.Drawing.Point(3, 14);
            this.lblRuntimeInfo.Name = "lblRuntimeInfo";
            this.lblRuntimeInfo.Size = new System.Drawing.Size(203, 18);
            this.lblRuntimeInfo.TabIndex = 0;
            this.lblRuntimeInfo.Text = "This panel populated at runtime";
            this.lblRuntimeInfo.Visible = false;
            // 
            // pUserSettings
            // 
            this.pUserSettings.Controls.Add(this.cb64bit);
            this.pUserSettings.Controls.Add(this.cbMinimizeOnLaunch);
            this.pUserSettings.Controls.Add(this.cbEnablePatchNotifications);
            this.pUserSettings.Controls.Add(this.btnLOTROPatchNow);
            this.pUserSettings.Controls.Add(this.btnDDOPatchNow);
            this.pUserSettings.Controls.Add(this.cbAutoPatch);
            this.pUserSettings.Controls.Add(this.btnFindLotro);
            this.pUserSettings.Controls.Add(this.btnFindDdo);
            this.pUserSettings.Controls.Add(this.lblLotroPath);
            this.pUserSettings.Controls.Add(this.btnUserSettingsSave);
            this.pUserSettings.Controls.Add(this.cbEnableLogging);
            this.pUserSettings.Controls.Add(this.cbKeepRawData);
            this.pUserSettings.Controls.Add(this.txtBxLOTROPath);
            this.pUserSettings.Controls.Add(this.txtBxDDOPath);
            this.pUserSettings.Controls.Add(this.lblDdoPath);
            this.pUserSettings.Location = new System.Drawing.Point(226, 137);
            this.pUserSettings.Name = "pUserSettings";
            this.pUserSettings.Size = new System.Drawing.Size(502, 286);
            this.pUserSettings.TabIndex = 6;
            // 
            // cbMinimizeOnLaunch
            // 
            this.cbMinimizeOnLaunch.AutoSize = true;
            this.cbMinimizeOnLaunch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbMinimizeOnLaunch.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cbMinimizeOnLaunch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cbMinimizeOnLaunch.Location = new System.Drawing.Point(134, 150);
            this.cbMinimizeOnLaunch.Name = "cbMinimizeOnLaunch";
            this.cbMinimizeOnLaunch.Size = new System.Drawing.Size(195, 22);
            this.cbMinimizeOnLaunch.TabIndex = 25;
            this.cbMinimizeOnLaunch.Text = "Minimize Main UI on Launch";
            this.cbMinimizeOnLaunch.UseVisualStyleBackColor = true;
            // 
            // cbEnablePatchNotifications
            // 
            this.cbEnablePatchNotifications.AutoSize = true;
            this.cbEnablePatchNotifications.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbEnablePatchNotifications.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cbEnablePatchNotifications.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cbEnablePatchNotifications.Location = new System.Drawing.Point(100, 92);
            this.cbEnablePatchNotifications.Name = "cbEnablePatchNotifications";
            this.cbEnablePatchNotifications.Size = new System.Drawing.Size(229, 22);
            this.cbEnablePatchNotifications.TabIndex = 22;
            this.cbEnablePatchNotifications.Text = "Enable Tray Pop-Up Notifications";
            this.cbEnablePatchNotifications.UseVisualStyleBackColor = true;
            // 
            // btnLOTROPatchNow
            // 
            this.btnLOTROPatchNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLOTROPatchNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnLOTROPatchNow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLOTROPatchNow.FlatAppearance.BorderSize = 0;
            this.btnLOTROPatchNow.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLOTROPatchNow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnLOTROPatchNow.Location = new System.Drawing.Point(385, 31);
            this.btnLOTROPatchNow.Margin = new System.Windows.Forms.Padding(0);
            this.btnLOTROPatchNow.Name = "btnLOTROPatchNow";
            this.btnLOTROPatchNow.Size = new System.Drawing.Size(98, 29);
            this.btnLOTROPatchNow.TabIndex = 21;
            this.btnLOTROPatchNow.Text = "Patch Now";
            this.btnLOTROPatchNow.UseVisualStyleBackColor = false;
            this.btnLOTROPatchNow.Click += new System.EventHandler(this.btnLOTROPatchNow_Click);
            // 
            // btnDDOPatchNow
            // 
            this.btnDDOPatchNow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDDOPatchNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnDDOPatchNow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDDOPatchNow.FlatAppearance.BorderSize = 0;
            this.btnDDOPatchNow.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDDOPatchNow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnDDOPatchNow.Location = new System.Drawing.Point(385, 2);
            this.btnDDOPatchNow.Margin = new System.Windows.Forms.Padding(0);
            this.btnDDOPatchNow.Name = "btnDDOPatchNow";
            this.btnDDOPatchNow.Size = new System.Drawing.Size(98, 29);
            this.btnDDOPatchNow.TabIndex = 20;
            this.btnDDOPatchNow.Text = "Patch Now";
            this.btnDDOPatchNow.UseVisualStyleBackColor = false;
            this.btnDDOPatchNow.Click += new System.EventHandler(this.btnDDOPatchNow_Click);
            // 
            // cbAutoPatch
            // 
            this.cbAutoPatch.AutoSize = true;
            this.cbAutoPatch.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbAutoPatch.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cbAutoPatch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cbAutoPatch.Location = new System.Drawing.Point(138, 64);
            this.cbAutoPatch.Name = "cbAutoPatch";
            this.cbAutoPatch.Size = new System.Drawing.Size(191, 22);
            this.cbAutoPatch.TabIndex = 19;
            this.cbAutoPatch.Text = "Patch before every Launch";
            this.cbAutoPatch.UseVisualStyleBackColor = true;
            // 
            // btnFindLotro
            // 
            this.btnFindLotro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnFindLotro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFindLotro.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold);
            this.btnFindLotro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnFindLotro.Location = new System.Drawing.Point(345, 34);
            this.btnFindLotro.Name = "btnFindLotro";
            this.btnFindLotro.Size = new System.Drawing.Size(37, 24);
            this.btnFindLotro.TabIndex = 18;
            this.btnFindLotro.Text = "...";
            this.btnFindLotro.UseVisualStyleBackColor = false;
            this.btnFindLotro.Click += new System.EventHandler(this.btnFindLotro_Click);
            // 
            // btnFindDdo
            // 
            this.btnFindDdo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnFindDdo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFindDdo.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold);
            this.btnFindDdo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnFindDdo.Location = new System.Drawing.Point(345, 5);
            this.btnFindDdo.Name = "btnFindDdo";
            this.btnFindDdo.Size = new System.Drawing.Size(37, 24);
            this.btnFindDdo.TabIndex = 17;
            this.btnFindDdo.Text = "...";
            this.btnFindDdo.UseVisualStyleBackColor = false;
            this.btnFindDdo.Click += new System.EventHandler(this.btnFindDdo_Click);
            // 
            // lblLotroPath
            // 
            this.lblLotroPath.AutoSize = true;
            this.lblLotroPath.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lblLotroPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lblLotroPath.Location = new System.Drawing.Point(3, 37);
            this.lblLotroPath.Name = "lblLotroPath";
            this.lblLotroPath.Size = new System.Drawing.Size(81, 18);
            this.lblLotroPath.TabIndex = 16;
            this.lblLotroPath.Text = "LOTRO Path";
            // 
            // btnUserSettingsSave
            // 
            this.btnUserSettingsSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUserSettingsSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnUserSettingsSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUserSettingsSave.FlatAppearance.BorderSize = 0;
            this.btnUserSettingsSave.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserSettingsSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnUserSettingsSave.Location = new System.Drawing.Point(403, 226);
            this.btnUserSettingsSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnUserSettingsSave.Name = "btnUserSettingsSave";
            this.btnUserSettingsSave.Size = new System.Drawing.Size(80, 38);
            this.btnUserSettingsSave.TabIndex = 15;
            this.btnUserSettingsSave.Text = "Save";
            this.btnUserSettingsSave.UseVisualStyleBackColor = false;
            this.btnUserSettingsSave.Click += new System.EventHandler(this.btnUserSettingsSave_Click);
            // 
            // cbEnableLogging
            // 
            this.cbEnableLogging.AutoSize = true;
            this.cbEnableLogging.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbEnableLogging.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cbEnableLogging.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cbEnableLogging.Location = new System.Drawing.Point(207, 206);
            this.cbEnableLogging.Name = "cbEnableLogging";
            this.cbEnableLogging.Size = new System.Drawing.Size(122, 22);
            this.cbEnableLogging.TabIndex = 14;
            this.cbEnableLogging.Text = "Enable Logging";
            this.cbEnableLogging.UseVisualStyleBackColor = true;
            this.cbEnableLogging.Visible = false;
            // 
            // cbKeepRawData
            // 
            this.cbKeepRawData.AutoSize = true;
            this.cbKeepRawData.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbKeepRawData.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cbKeepRawData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cbKeepRawData.Location = new System.Drawing.Point(208, 178);
            this.cbKeepRawData.Name = "cbKeepRawData";
            this.cbKeepRawData.Size = new System.Drawing.Size(121, 22);
            this.cbKeepRawData.TabIndex = 13;
            this.cbKeepRawData.Text = "Keep Raw Data";
            this.cbKeepRawData.UseVisualStyleBackColor = true;
            this.cbKeepRawData.Visible = false;
            // 
            // txtBxLOTROPath
            // 
            this.txtBxLOTROPath.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.txtBxLOTROPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.txtBxLOTROPath.Location = new System.Drawing.Point(118, 34);
            this.txtBxLOTROPath.Name = "txtBxLOTROPath";
            this.txtBxLOTROPath.Size = new System.Drawing.Size(221, 24);
            this.txtBxLOTROPath.TabIndex = 8;
            // 
            // txtBxDDOPath
            // 
            this.txtBxDDOPath.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.txtBxDDOPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.txtBxDDOPath.Location = new System.Drawing.Point(118, 5);
            this.txtBxDDOPath.Name = "txtBxDDOPath";
            this.txtBxDDOPath.Size = new System.Drawing.Size(221, 24);
            this.txtBxDDOPath.TabIndex = 7;
            // 
            // lblDdoPath
            // 
            this.lblDdoPath.AutoSize = true;
            this.lblDdoPath.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lblDdoPath.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lblDdoPath.Location = new System.Drawing.Point(3, 8);
            this.lblDdoPath.Name = "lblDdoPath";
            this.lblDdoPath.Size = new System.Drawing.Size(66, 18);
            this.lblDdoPath.TabIndex = 0;
            this.lblDdoPath.Text = "DDO Path";
            // 
            // pEditAccount
            // 
            this.pEditAccount.Controls.Add(this.btnRemoveAccount);
            this.pEditAccount.Controls.Add(this.tbEditDisplayName);
            this.pEditAccount.Controls.Add(this.lEditDisplay);
            this.pEditAccount.Controls.Add(this.btnEditAccountCancel);
            this.pEditAccount.Controls.Add(this.btnEditAccountSave);
            this.pEditAccount.Controls.Add(this.tbEditPassword);
            this.pEditAccount.Controls.Add(this.lEditPassword);
            this.pEditAccount.Controls.Add(this.tbEditUsername);
            this.pEditAccount.Controls.Add(this.lEditUsername);
            this.pEditAccount.Location = new System.Drawing.Point(791, 218);
            this.pEditAccount.Margin = new System.Windows.Forms.Padding(0);
            this.pEditAccount.Name = "pEditAccount";
            this.pEditAccount.Size = new System.Drawing.Size(394, 314);
            this.pEditAccount.TabIndex = 3;
            this.pEditAccount.Visible = false;
            // 
            // btnRemoveAccount
            // 
            this.btnRemoveAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnRemoveAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemoveAccount.FlatAppearance.BorderSize = 0;
            this.btnRemoveAccount.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnRemoveAccount.Location = new System.Drawing.Point(282, 96);
            this.btnRemoveAccount.Margin = new System.Windows.Forms.Padding(0);
            this.btnRemoveAccount.Name = "btnRemoveAccount";
            this.btnRemoveAccount.Size = new System.Drawing.Size(80, 38);
            this.btnRemoveAccount.TabIndex = 7;
            this.btnRemoveAccount.Text = "Remove";
            this.btnRemoveAccount.UseVisualStyleBackColor = false;
            this.btnRemoveAccount.Click += new System.EventHandler(this.btnRemoveAccount_Click);
            // 
            // tbEditDisplayName
            // 
            this.tbEditDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbEditDisplayName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbEditDisplayName.Location = new System.Drawing.Point(122, 39);
            this.tbEditDisplayName.Name = "tbEditDisplayName";
            this.tbEditDisplayName.Size = new System.Drawing.Size(240, 24);
            this.tbEditDisplayName.TabIndex = 2;
            // 
            // lEditDisplay
            // 
            this.lEditDisplay.AutoSize = true;
            this.lEditDisplay.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lEditDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lEditDisplay.Location = new System.Drawing.Point(3, 42);
            this.lEditDisplay.Name = "lEditDisplay";
            this.lEditDisplay.Size = new System.Drawing.Size(91, 18);
            this.lEditDisplay.TabIndex = 6;
            this.lEditDisplay.Text = "Display Name";
            // 
            // btnEditAccountCancel
            // 
            this.btnEditAccountCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnEditAccountCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditAccountCancel.FlatAppearance.BorderSize = 0;
            this.btnEditAccountCancel.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAccountCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnEditAccountCancel.Location = new System.Drawing.Point(202, 96);
            this.btnEditAccountCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnEditAccountCancel.Name = "btnEditAccountCancel";
            this.btnEditAccountCancel.Size = new System.Drawing.Size(80, 38);
            this.btnEditAccountCancel.TabIndex = 5;
            this.btnEditAccountCancel.Text = "Cancel";
            this.btnEditAccountCancel.UseVisualStyleBackColor = false;
            this.btnEditAccountCancel.Click += new System.EventHandler(this.btnEditAccountCancel_Click);
            // 
            // btnEditAccountSave
            // 
            this.btnEditAccountSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnEditAccountSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditAccountSave.FlatAppearance.BorderSize = 0;
            this.btnEditAccountSave.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAccountSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnEditAccountSave.Location = new System.Drawing.Point(122, 96);
            this.btnEditAccountSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnEditAccountSave.Name = "btnEditAccountSave";
            this.btnEditAccountSave.Size = new System.Drawing.Size(80, 38);
            this.btnEditAccountSave.TabIndex = 4;
            this.btnEditAccountSave.Text = "Save";
            this.btnEditAccountSave.UseVisualStyleBackColor = false;
            this.btnEditAccountSave.Click += new System.EventHandler(this.btnEditAccountSave_Click);
            // 
            // tbEditPassword
            // 
            this.tbEditPassword.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbEditPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbEditPassword.Location = new System.Drawing.Point(122, 69);
            this.tbEditPassword.Name = "tbEditPassword";
            this.tbEditPassword.PasswordChar = '*';
            this.tbEditPassword.Size = new System.Drawing.Size(240, 24);
            this.tbEditPassword.TabIndex = 3;
            // 
            // lEditPassword
            // 
            this.lEditPassword.AutoSize = true;
            this.lEditPassword.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lEditPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lEditPassword.Location = new System.Drawing.Point(3, 72);
            this.lEditPassword.Name = "lEditPassword";
            this.lEditPassword.Size = new System.Drawing.Size(69, 18);
            this.lEditPassword.TabIndex = 2;
            this.lEditPassword.Text = "Password";
            // 
            // tbEditUsername
            // 
            this.tbEditUsername.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbEditUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbEditUsername.Location = new System.Drawing.Point(122, 9);
            this.tbEditUsername.Name = "tbEditUsername";
            this.tbEditUsername.ReadOnly = true;
            this.tbEditUsername.Size = new System.Drawing.Size(240, 24);
            this.tbEditUsername.TabIndex = 1;
            // 
            // lEditUsername
            // 
            this.lEditUsername.AutoSize = true;
            this.lEditUsername.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lEditUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lEditUsername.Location = new System.Drawing.Point(3, 12);
            this.lEditUsername.Name = "lEditUsername";
            this.lEditUsername.Size = new System.Drawing.Size(71, 18);
            this.lEditUsername.TabIndex = 0;
            this.lEditUsername.Text = "Username";
            // 
            // scAccountDetails
            // 
            this.scAccountDetails.Location = new System.Drawing.Point(3, 3);
            this.scAccountDetails.Name = "scAccountDetails";
            this.scAccountDetails.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAccountDetails.Panel1
            // 
            this.scAccountDetails.Panel1.Controls.Add(this.tbShortcutDisplayName);
            this.scAccountDetails.Panel1.Controls.Add(this.btnCancelShortcut);
            this.scAccountDetails.Panel1.Controls.Add(this.btnAddShortcut);
            this.scAccountDetails.Panel1.Controls.Add(this.tbAddShortcutCharacter);
            this.scAccountDetails.Panel1.Controls.Add(this.cbMakeDefault);
            this.scAccountDetails.Panel1.Controls.Add(this.btnPlay);
            this.scAccountDetails.Panel1.Controls.Add(this.cbPlayServers);
            this.scAccountDetails.Panel1MinSize = 45;
            // 
            // scAccountDetails.Panel2
            // 
            this.scAccountDetails.Panel2.Controls.Add(this.scAccountDetailsLower);
            this.scAccountDetails.Size = new System.Drawing.Size(556, 500);
            this.scAccountDetails.SplitterDistance = 105;
            this.scAccountDetails.TabIndex = 5;
            this.scAccountDetails.Visible = false;
            // 
            // tbShortcutDisplayName
            // 
            this.tbShortcutDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbShortcutDisplayName.Location = new System.Drawing.Point(6, 72);
            this.tbShortcutDisplayName.Name = "tbShortcutDisplayName";
            this.tbShortcutDisplayName.Size = new System.Drawing.Size(247, 24);
            this.tbShortcutDisplayName.TabIndex = 13;
            // 
            // btnCancelShortcut
            // 
            this.btnCancelShortcut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelShortcut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnCancelShortcut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelShortcut.FlatAppearance.BorderSize = 0;
            this.btnCancelShortcut.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelShortcut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnCancelShortcut.Location = new System.Drawing.Point(336, 68);
            this.btnCancelShortcut.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancelShortcut.Name = "btnCancelShortcut";
            this.btnCancelShortcut.Size = new System.Drawing.Size(62, 30);
            this.btnCancelShortcut.TabIndex = 12;
            this.btnCancelShortcut.Text = "Cancel";
            this.btnCancelShortcut.UseVisualStyleBackColor = false;
            this.btnCancelShortcut.Visible = false;
            this.btnCancelShortcut.Click += new System.EventHandler(this.btnCancelShortcut_Click);
            // 
            // btnAddShortcut
            // 
            this.btnAddShortcut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddShortcut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnAddShortcut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddShortcut.FlatAppearance.BorderSize = 0;
            this.btnAddShortcut.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddShortcut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnAddShortcut.Location = new System.Drawing.Point(398, 68);
            this.btnAddShortcut.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddShortcut.Name = "btnAddShortcut";
            this.btnAddShortcut.Size = new System.Drawing.Size(154, 30);
            this.btnAddShortcut.TabIndex = 11;
            this.btnAddShortcut.Text = "Create Shortcut";
            this.btnAddShortcut.UseVisualStyleBackColor = false;
            this.btnAddShortcut.Click += new System.EventHandler(this.tbnAddUpdateShortcut_Click);
            // 
            // tbAddShortcutCharacter
            // 
            this.tbAddShortcutCharacter.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAddShortcutCharacter.Location = new System.Drawing.Point(6, 42);
            this.tbAddShortcutCharacter.Name = "tbAddShortcutCharacter";
            this.tbAddShortcutCharacter.Size = new System.Drawing.Size(247, 24);
            this.tbAddShortcutCharacter.TabIndex = 10;
            // 
            // cbMakeDefault
            // 
            this.cbMakeDefault.AutoSize = true;
            this.cbMakeDefault.Location = new System.Drawing.Point(259, 16);
            this.cbMakeDefault.Name = "cbMakeDefault";
            this.cbMakeDefault.Size = new System.Drawing.Size(95, 18);
            this.cbMakeDefault.TabIndex = 6;
            this.cbMakeDefault.Text = "Set as Default";
            this.cbMakeDefault.UseVisualStyleBackColor = true;
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlay.FlatAppearance.BorderSize = 0;
            this.btnPlay.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnPlay.Location = new System.Drawing.Point(446, 8);
            this.btnPlay.Margin = new System.Windows.Forms.Padding(0);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(107, 30);
            this.btnPlay.TabIndex = 5;
            this.btnPlay.Text = "Play Now!";
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // cbPlayServers
            // 
            this.cbPlayServers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlayServers.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPlayServers.FormattingEnabled = true;
            this.cbPlayServers.Location = new System.Drawing.Point(6, 12);
            this.cbPlayServers.Name = "cbPlayServers";
            this.cbPlayServers.Size = new System.Drawing.Size(247, 24);
            this.cbPlayServers.TabIndex = 0;
            // 
            // scAccountDetailsLower
            // 
            this.scAccountDetailsLower.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scAccountDetailsLower.Location = new System.Drawing.Point(0, 0);
            this.scAccountDetailsLower.Name = "scAccountDetailsLower";
            this.scAccountDetailsLower.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAccountDetailsLower.Panel1
            // 
            this.scAccountDetailsLower.Panel1.Controls.Add(this.pShortcuts);
            // 
            // scAccountDetailsLower.Panel2
            // 
            this.scAccountDetailsLower.Panel2.Controls.Add(this.btnEditSubSave);
            this.scAccountDetailsLower.Panel2.Controls.Add(this.tbEditSubDisplayName);
            this.scAccountDetailsLower.Panel2.Controls.Add(this.lblEditSubDisplayName);
            this.scAccountDetailsLower.Panel2.Controls.Add(this.lblSubscriptionInfo);
            this.scAccountDetailsLower.Panel2MinSize = 75;
            this.scAccountDetailsLower.Size = new System.Drawing.Size(556, 391);
            this.scAccountDetailsLower.SplitterDistance = 312;
            this.scAccountDetailsLower.TabIndex = 0;
            // 
            // pShortcuts
            // 
            this.pShortcuts.Controls.Add(this.lblShortcuts);
            this.pShortcuts.Location = new System.Drawing.Point(0, 0);
            this.pShortcuts.Name = "pShortcuts";
            this.pShortcuts.Size = new System.Drawing.Size(472, 308);
            this.pShortcuts.TabIndex = 0;
            // 
            // lblShortcuts
            // 
            this.lblShortcuts.AutoSize = true;
            this.lblShortcuts.Font = new System.Drawing.Font("Neo Sans", 12F, System.Drawing.FontStyle.Bold);
            this.lblShortcuts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lblShortcuts.Location = new System.Drawing.Point(3, 3);
            this.lblShortcuts.Name = "lblShortcuts";
            this.lblShortcuts.Size = new System.Drawing.Size(204, 20);
            this.lblShortcuts.TabIndex = 10;
            this.lblShortcuts.Text = "Shortcuts (Click to Play!)";
            // 
            // btnEditSubSave
            // 
            this.btnEditSubSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnEditSubSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditSubSave.FlatAppearance.BorderSize = 0;
            this.btnEditSubSave.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditSubSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnEditSubSave.Location = new System.Drawing.Point(384, 31);
            this.btnEditSubSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnEditSubSave.Name = "btnEditSubSave";
            this.btnEditSubSave.Size = new System.Drawing.Size(80, 38);
            this.btnEditSubSave.TabIndex = 10;
            this.btnEditSubSave.Text = "Save";
            this.btnEditSubSave.UseVisualStyleBackColor = false;
            this.btnEditSubSave.Click += new System.EventHandler(this.btnEditSubSave_Click);
            // 
            // tbEditSubDisplayName
            // 
            this.tbEditSubDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbEditSubDisplayName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbEditSubDisplayName.Location = new System.Drawing.Point(101, 39);
            this.tbEditSubDisplayName.Name = "tbEditSubDisplayName";
            this.tbEditSubDisplayName.Size = new System.Drawing.Size(277, 24);
            this.tbEditSubDisplayName.TabIndex = 9;
            // 
            // lblEditSubDisplayName
            // 
            this.lblEditSubDisplayName.AutoSize = true;
            this.lblEditSubDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lblEditSubDisplayName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lblEditSubDisplayName.Location = new System.Drawing.Point(4, 42);
            this.lblEditSubDisplayName.Name = "lblEditSubDisplayName";
            this.lblEditSubDisplayName.Size = new System.Drawing.Size(91, 18);
            this.lblEditSubDisplayName.TabIndex = 8;
            this.lblEditSubDisplayName.Text = "Display Name";
            // 
            // lblSubscriptionInfo
            // 
            this.lblSubscriptionInfo.AutoSize = true;
            this.lblSubscriptionInfo.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubscriptionInfo.ForeColor = System.Drawing.Color.Gray;
            this.lblSubscriptionInfo.Location = new System.Drawing.Point(10, 10);
            this.lblSubscriptionInfo.Name = "lblSubscriptionInfo";
            this.lblSubscriptionInfo.Size = new System.Drawing.Size(179, 18);
            this.lblSubscriptionInfo.TabIndex = 7;
            this.lblSubscriptionInfo.Text = "Subscription Info goes here";
            // 
            // pMessages
            // 
            this.pMessages.Controls.Add(this.btnMessaagesOkay);
            this.pMessages.Controls.Add(this.lblMessage);
            this.pMessages.Location = new System.Drawing.Point(743, 508);
            this.pMessages.Margin = new System.Windows.Forms.Padding(0);
            this.pMessages.Name = "pMessages";
            this.pMessages.Size = new System.Drawing.Size(360, 142);
            this.pMessages.TabIndex = 2;
            this.pMessages.Visible = false;
            // 
            // btnMessaagesOkay
            // 
            this.btnMessaagesOkay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnMessaagesOkay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMessaagesOkay.FlatAppearance.BorderSize = 0;
            this.btnMessaagesOkay.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMessaagesOkay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnMessaagesOkay.Location = new System.Drawing.Point(203, 87);
            this.btnMessaagesOkay.Margin = new System.Windows.Forms.Padding(0);
            this.btnMessaagesOkay.Name = "btnMessaagesOkay";
            this.btnMessaagesOkay.Size = new System.Drawing.Size(89, 38);
            this.btnMessaagesOkay.TabIndex = 5;
            this.btnMessaagesOkay.Text = "Continue";
            this.btnMessaagesOkay.UseVisualStyleBackColor = false;
            this.btnMessaagesOkay.Click += new System.EventHandler(this.btnMessagesOkay_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lblMessage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lblMessage.Location = new System.Drawing.Point(3, 6);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(194, 18);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "This is a message placeholder.";
            this.lblMessage.Visible = false;
            // 
            // pAddAccount
            // 
            this.pAddAccount.Controls.Add(this.tbAccountDisplayName);
            this.pAddAccount.Controls.Add(this.lDisplayName);
            this.pAddAccount.Controls.Add(this.btnAccountCancel);
            this.pAddAccount.Controls.Add(this.btnAccountSave);
            this.pAddAccount.Controls.Add(this.tbAccountPassword);
            this.pAddAccount.Controls.Add(this.lPassword);
            this.pAddAccount.Controls.Add(this.tbAccountUsername);
            this.pAddAccount.Controls.Add(this.lUsername);
            this.pAddAccount.Location = new System.Drawing.Point(607, 18);
            this.pAddAccount.Margin = new System.Windows.Forms.Padding(0);
            this.pAddAccount.Name = "pAddAccount";
            this.pAddAccount.Size = new System.Drawing.Size(496, 152);
            this.pAddAccount.TabIndex = 0;
            this.pAddAccount.Visible = false;
            // 
            // tbAccountDisplayName
            // 
            this.tbAccountDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbAccountDisplayName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountDisplayName.Location = new System.Drawing.Point(122, 41);
            this.tbAccountDisplayName.Name = "tbAccountDisplayName";
            this.tbAccountDisplayName.Size = new System.Drawing.Size(222, 24);
            this.tbAccountDisplayName.TabIndex = 2;
            // 
            // lDisplayName
            // 
            this.lDisplayName.AutoSize = true;
            this.lDisplayName.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lDisplayName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lDisplayName.Location = new System.Drawing.Point(3, 44);
            this.lDisplayName.Name = "lDisplayName";
            this.lDisplayName.Size = new System.Drawing.Size(91, 18);
            this.lDisplayName.TabIndex = 6;
            this.lDisplayName.Text = "Display Name";
            // 
            // btnAccountCancel
            // 
            this.btnAccountCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnAccountCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccountCancel.FlatAppearance.BorderSize = 0;
            this.btnAccountCancel.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccountCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnAccountCancel.Location = new System.Drawing.Point(264, 98);
            this.btnAccountCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnAccountCancel.Name = "btnAccountCancel";
            this.btnAccountCancel.Size = new System.Drawing.Size(80, 38);
            this.btnAccountCancel.TabIndex = 5;
            this.btnAccountCancel.Text = "Cancel";
            this.btnAccountCancel.UseVisualStyleBackColor = false;
            this.btnAccountCancel.Click += new System.EventHandler(this.btnAccountCancel_Click);
            // 
            // btnAccountSave
            // 
            this.btnAccountSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnAccountSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccountSave.FlatAppearance.BorderSize = 0;
            this.btnAccountSave.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccountSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnAccountSave.Location = new System.Drawing.Point(184, 98);
            this.btnAccountSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnAccountSave.Name = "btnAccountSave";
            this.btnAccountSave.Size = new System.Drawing.Size(80, 38);
            this.btnAccountSave.TabIndex = 4;
            this.btnAccountSave.Text = "Save";
            this.btnAccountSave.UseVisualStyleBackColor = false;
            this.btnAccountSave.Click += new System.EventHandler(this.btnAccountSave_Click);
            // 
            // tbAccountPassword
            // 
            this.tbAccountPassword.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbAccountPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountPassword.Location = new System.Drawing.Point(122, 71);
            this.tbAccountPassword.Name = "tbAccountPassword";
            this.tbAccountPassword.PasswordChar = '*';
            this.tbAccountPassword.Size = new System.Drawing.Size(222, 24);
            this.tbAccountPassword.TabIndex = 3;
            // 
            // lPassword
            // 
            this.lPassword.AutoSize = true;
            this.lPassword.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lPassword.Location = new System.Drawing.Point(3, 74);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(69, 18);
            this.lPassword.TabIndex = 2;
            this.lPassword.Text = "Password";
            // 
            // tbAccountUsername
            // 
            this.tbAccountUsername.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.tbAccountUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.tbAccountUsername.Location = new System.Drawing.Point(122, 11);
            this.tbAccountUsername.Name = "tbAccountUsername";
            this.tbAccountUsername.Size = new System.Drawing.Size(222, 24);
            this.tbAccountUsername.TabIndex = 1;
            this.tbAccountUsername.Leave += new System.EventHandler(this.tbAccountUsername_Leave);
            // 
            // lUsername
            // 
            this.lUsername.AutoSize = true;
            this.lUsername.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.lUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.lUsername.Location = new System.Drawing.Point(3, 14);
            this.lUsername.Name = "lUsername";
            this.lUsername.Size = new System.Drawing.Size(71, 18);
            this.lUsername.TabIndex = 0;
            this.lUsername.Text = "Username";
            // 
            // pMainMenu
            // 
            this.pMainMenu.Controls.Add(this.btnRefreshStatus);
            this.pMainMenu.Controls.Add(this.lblServerStatus);
            this.pMainMenu.Location = new System.Drawing.Point(759, 349);
            this.pMainMenu.Name = "pMainMenu";
            this.pMainMenu.Size = new System.Drawing.Size(272, 164);
            this.pMainMenu.TabIndex = 4;
            this.pMainMenu.Visible = false;
            // 
            // btnRefreshStatus
            // 
            this.btnRefreshStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.btnRefreshStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefreshStatus.FlatAppearance.BorderSize = 0;
            this.btnRefreshStatus.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefreshStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(252)))), ((int)(((byte)(254)))));
            this.btnRefreshStatus.Location = new System.Drawing.Point(171, 121);
            this.btnRefreshStatus.Margin = new System.Windows.Forms.Padding(0);
            this.btnRefreshStatus.Name = "btnRefreshStatus";
            this.btnRefreshStatus.Size = new System.Drawing.Size(93, 38);
            this.btnRefreshStatus.TabIndex = 5;
            this.btnRefreshStatus.Text = "Refresh";
            this.btnRefreshStatus.UseVisualStyleBackColor = false;
            this.btnRefreshStatus.Click += new System.EventHandler(this.btnRefreshStatus_Click);
            // 
            // lblServerStatus
            // 
            this.lblServerStatus.AutoSize = true;
            this.lblServerStatus.Font = new System.Drawing.Font("Neo Sans", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerStatus.Location = new System.Drawing.Point(8, 6);
            this.lblServerStatus.Name = "lblServerStatus";
            this.lblServerStatus.Size = new System.Drawing.Size(145, 18);
            this.lblServerStatus.TabIndex = 0;
            this.lblServerStatus.Text = "Replace me at runtime";
            // 
            // ilMain
            // 
            this.ilMain.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilMain.ImageSize = new System.Drawing.Size(36, 36);
            this.ilMain.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmsDDO
            // 
            this.cmsDDO.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmsDDO.Name = "cmsDDO";
            this.cmsDDO.Size = new System.Drawing.Size(61, 4);
            // 
            // cmsLotro
            // 
            this.cmsLotro.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmsLotro.Name = "cmsLotro";
            this.cmsLotro.Size = new System.Drawing.Size(61, 4);
            // 
            // cb64bit
            // 
            this.cb64bit.AutoSize = true;
            this.cb64bit.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb64bit.Font = new System.Drawing.Font("Neo Sans", 10F);
            this.cb64bit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(25)))), ((int)(((byte)(49)))));
            this.cb64bit.Location = new System.Drawing.Point(187, 121);
            this.cb64bit.Name = "cb64bit";
            this.cb64bit.Size = new System.Drawing.Size(142, 22);
            this.cb64bit.TabIndex = 26;
            this.cb64bit.Text = "Use 64-bit client(s)";
            this.cb64bit.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(36)))), ((int)(((byte)(79)))));
            this.ClientSize = new System.Drawing.Size(1104, 628);
            this.Controls.Add(this.scHeader);
            this.Font = new System.Drawing.Font("Neo Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.scHeader.Panel1.ResumeLayout(false);
            this.scHeader.Panel1.PerformLayout();
            this.scHeader.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scHeader)).EndInit();
            this.scHeader.ResumeLayout(false);
            this.scNav.Panel1.ResumeLayout(false);
            this.scNav.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scNav)).EndInit();
            this.scNav.ResumeLayout(false);
            this.scMain.Panel1.ResumeLayout(false);
            this.scMain.Panel1.PerformLayout();
            this.scMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMain)).EndInit();
            this.scMain.ResumeLayout(false);
            this.pSettings.ResumeLayout(false);
            this.pHome.ResumeLayout(false);
            this.pAccounts.ResumeLayout(false);
            this.pAccounts.PerformLayout();
            this.pUserSettings.ResumeLayout(false);
            this.pUserSettings.PerformLayout();
            this.pEditAccount.ResumeLayout(false);
            this.pEditAccount.PerformLayout();
            this.scAccountDetails.Panel1.ResumeLayout(false);
            this.scAccountDetails.Panel1.PerformLayout();
            this.scAccountDetails.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scAccountDetails)).EndInit();
            this.scAccountDetails.ResumeLayout(false);
            this.scAccountDetailsLower.Panel1.ResumeLayout(false);
            this.scAccountDetailsLower.Panel2.ResumeLayout(false);
            this.scAccountDetailsLower.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAccountDetailsLower)).EndInit();
            this.scAccountDetailsLower.ResumeLayout(false);
            this.pShortcuts.ResumeLayout(false);
            this.pShortcuts.PerformLayout();
            this.pMessages.ResumeLayout(false);
            this.pMessages.PerformLayout();
            this.pAddAccount.ResumeLayout(false);
            this.pAddAccount.PerformLayout();
            this.pMainMenu.ResumeLayout(false);
            this.pMainMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scHeader;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.SplitContainer scNav;
        private System.Windows.Forms.SplitContainer scMain;
        private System.Windows.Forms.ImageList ilMain;
        private System.Windows.Forms.Button btnAccounts;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel pAccounts;
        private System.Windows.Forms.Panel pSettings;
        private System.Windows.Forms.Panel pHome;
        private System.Windows.Forms.Button btnGitlab;
        private System.Windows.Forms.Button btnDiscord;
        private System.Windows.Forms.Button btnWeb;
        private System.Windows.Forms.Button btnPatch;
        private System.Windows.Forms.Panel pAddAccount;
        private System.Windows.Forms.Label lUsername;
        private System.Windows.Forms.Button btnAccountCancel;
        private System.Windows.Forms.Button btnAccountSave;
        private System.Windows.Forms.TextBox tbAccountPassword;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.TextBox tbAccountUsername;
        private System.Windows.Forms.TextBox tbAccountDisplayName;
        private System.Windows.Forms.Label lDisplayName;
        private System.Windows.Forms.Panel pMessages;
        private System.Windows.Forms.Button btnMessaagesOkay;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Panel pEditAccount;
        private System.Windows.Forms.TextBox tbEditDisplayName;
        private System.Windows.Forms.Label lEditDisplay;
        private System.Windows.Forms.Button btnEditAccountCancel;
        private System.Windows.Forms.Button btnEditAccountSave;
        private System.Windows.Forms.TextBox tbEditPassword;
        private System.Windows.Forms.Label lEditPassword;
        private System.Windows.Forms.TextBox tbEditUsername;
        private System.Windows.Forms.Label lEditUsername;
        private System.Windows.Forms.ContextMenuStrip cmsDDO;
        private System.Windows.Forms.ContextMenuStrip cmsLotro;
        private System.Windows.Forms.Panel pMainMenu;
        private System.Windows.Forms.Button btnRefreshStatus;
        private System.Windows.Forms.Label lblServerStatus;
        private System.Windows.Forms.SplitContainer scAccountDetails;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.ComboBox cbPlayServers;
        private System.Windows.Forms.SplitContainer scAccountDetailsLower;
        private System.Windows.Forms.Label lblSubscriptionInfo;
        private System.Windows.Forms.CheckBox cbMakeDefault;
        private System.Windows.Forms.Button btnRemoveAccount;
        private System.Windows.Forms.Button btnEditSubSave;
        private System.Windows.Forms.TextBox tbEditSubDisplayName;
        private System.Windows.Forms.Label lblEditSubDisplayName;
        private System.Windows.Forms.Button btnAddShortcut;
        private System.Windows.Forms.TextBox tbAddShortcutCharacter;
        private System.Windows.Forms.Panel pShortcuts;
        private System.Windows.Forms.Label lblShortcuts;
        private System.Windows.Forms.Panel pUserSettings;
        private System.Windows.Forms.Label lblDdoPath;
        private System.Windows.Forms.TextBox txtBxLOTROPath;
        private System.Windows.Forms.TextBox txtBxDDOPath;
        private System.Windows.Forms.CheckBox cbEnableLogging;
        private System.Windows.Forms.CheckBox cbKeepRawData;
        private System.Windows.Forms.Button btnUserSettingsSave;
        private System.Windows.Forms.Label lblLotroPath;
        private System.Windows.Forms.Button btnFindLotro;
        private System.Windows.Forms.Button btnFindDdo;
        private System.Windows.Forms.Label lblRuntimeInfo;
        private System.Windows.Forms.CheckBox cbAutoPatch;
        private System.Windows.Forms.Button btnDDOPatchNow;
        private System.Windows.Forms.Button btnLOTROPatchNow;
        private System.Windows.Forms.Button btnWikiPage;
        private System.Windows.Forms.CheckBox cbEnablePatchNotifications;
        private System.Windows.Forms.CheckBox cbMinimizeOnLaunch;
        private System.Windows.Forms.Button btnCancelShortcut;
        private System.Windows.Forms.TextBox tbShortcutDisplayName;
        private System.Windows.Forms.CheckBox cb64bit;
    }
}