﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace VoK.TrayApp
{
    public partial class TextOutputWindow : Form
    {
        private bool _pauseAfterExecution;

        public TextOutputWindow()
        {
            InitializeComponent();
            DialogResult = DialogResult.OK;
        }

        public TextOutputWindow(bool pauseAfterExecution, string buttonText = null)
        {
            InitializeComponent();

            // This prevents the dialog box from closing until the "Okay/Continue" button is pressed.
            if (pauseAfterExecution)
            {
                _pauseAfterExecution = true;
                DialogResult = DialogResult.Retry;
            }
            else
            {
                // Setting this causes no wait after execution:
                DialogResult = DialogResult.OK;
            }

            if (!string.IsNullOrWhiteSpace(buttonText))
                btnOkay.Text = buttonText;
        }

        public static void ShowDialog(string dialogText, string buttonText)
        {
            var window = new TextOutputWindow(true, buttonText);
            window.tbPatchOutput.Text = dialogText;
            window.btnOkay.Text = buttonText;
            window.ShowDialog();
        }

        public void EnableButtons()
        {
            if (DialogResult == DialogResult.Retry || DialogResult == DialogResult.None)
            {
                btnOkay.Text = "Continue";
                _pauseAfterExecution = true;
            }
        }

        public void AppendPatchChar(char text)
        {
#if DEBUG
            Debug.Write(text);
#endif
            tbPatchOutput.Invoke((MethodInvoker)(() =>
            {
                tbPatchOutput.AppendText(text.ToString());
            }));

            Application.DoEvents();
        }

        public void AppendString(string text)
        {
#if DEBUG
            Debug.Write(text);
#endif
            tbPatchOutput.Invoke((MethodInvoker)(() =>
            {
                tbPatchOutput.AppendText(text);
            }));

            Application.DoEvents();
        }

        private void btnOkay_Click(object sender, EventArgs e)
        {
            if (_pauseAfterExecution)
                // Changes to OK, to close the window from the parent thread.
                DialogResult = DialogResult.OK;
        }

        private void scHeaderPanel1_OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MainForm.ReleaseCapture();
                MainForm.SendMessage(Handle, MainForm.WM_NCLBUTTONDOWN, MainForm.HT_CAPTION, null);
            }
        }
    }
}
