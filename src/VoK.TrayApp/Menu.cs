﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using VoK.TrayApp.Properties;
using System.Collections.Generic;

namespace VoK.TrayApp
{
    internal static class Menu
    {
        private static Action _onChange;

        public static ContextMenuStrip Initialize(Action onChange)
        {
            DrawMenu();
            _onChange = onChange;
            SettingsFactoryHost.DefaultSettingsFactory.OnChange += DefaultSettingsFactory_OnChange;

            return MainMenu;
        }

        private static void DefaultSettingsFactory_OnChange(object sender, EventArgs e)
        {
            DrawMenu();
            _onChange();
        }

        public static ContextMenuStrip MainMenu { get; private set; }

        private static void DrawMenu()
        {
            MainMenu = new ContextMenuStrip();

            ToolStripMenuItem trayItem;

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            var allShortcuts = new List<Tuple<Subscription, Shortcut>>();

            foreach (var account in settings.StoredAccounts)
            {
                foreach (var sub in account.Subscriptions)
                {
                    foreach (var shortcut in sub.Shortcuts)
                    {
                        string text = shortcut.DisplayName;

                        if (string.IsNullOrWhiteSpace(text))
                        {
                            text = string.Empty;

                            if (settings.StoredAccounts.Count > 1)
                                text += account.DisplayName + " - ";

                            if (account.Subscriptions.Count > 1)
                                text += sub.DisplayName + " - ";

                            text += shortcut.Server;

                            if (!string.IsNullOrWhiteSpace(shortcut.Character))
                                text += " - " + shortcut.Character;
                        }

                        allShortcuts.Add(Tuple.Create(sub, new Shortcut() { Character = shortcut.Character, Server = shortcut.Server, DisplayName = text }));
                    }
                }
            }

            allShortcuts = allShortcuts.OrderBy(tup => tup.Item2.GetDisplayName()).ToList();

            foreach (var tuple in allShortcuts)
            {
                Image icon = null;
                if (tuple.Item1.Game == GameId.DDO)
                    icon = Resources.ddo_16x16;
                else if (tuple.Item1.Game == GameId.LOTRO)
                    icon = Resources.lotro_16x16;


                trayItem = new ToolStripMenuItem { Text = tuple.Item2.DisplayName, Image = icon };
                trayItem.Click += LaunchShortcut_Click;
                trayItem.Tag = Tuple.Create(tuple.Item1, tuple.Item2);
                MainMenu.Items.Add(trayItem);
            }

            MainMenu.Items.Add(new ToolStripSeparator());

            // Open the Main Form
            trayItem = new ToolStripMenuItem { Text = @"Open Main UI" };
            trayItem.Click += MainForm_Click;
            MainMenu.Items.Add(trayItem);

            // Exit
            trayItem = new ToolStripMenuItem { Text = @"Exit" };
            trayItem.Click += Exit_Click;
            MainMenu.Items.Add(trayItem);
        }

        private static void LaunchShortcut_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tray = (ToolStripMenuItem)sender;

            if (tray.Tag != null)
            {
                var tup = tray.Tag as Tuple<Subscription, Shortcut>;
                Thread t = new Thread(() => Program.MainForm.Play(tup?.Item1, tup?.Item2));
                t.Name = "TrayPlay";
                t.IsBackground = true;
                t.Start();
            }
        }
        
        private static void MainForm_Click(object sender, EventArgs e)
        {
                Program.MainForm.Show();
                Program.MainForm.BringToFront();
        }

        private static void Exit_Click(object sender, EventArgs e)
        {
            Program.Exit();
        }
    }
}