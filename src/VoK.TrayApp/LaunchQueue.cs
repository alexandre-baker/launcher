﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace VoK.TrayApp
{
    public class LaunchQueue
    {
        private static Thread _launchWaitThread = null;
        private static ConcurrentQueue<LaunchRequest> _launchQueue;
        private const int DELAY_BETWEEN_LAUNCHES = 10000;
        private const int DELAY_BETWEEN_PATCH_CHECKS = 1000;
        private const int DELAY_BETWEEN_QUEUE_CHECKS = 1000;

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static LaunchQueue()
        {
            _launchQueue = new ConcurrentQueue<LaunchRequest>();

            _launchWaitThread = new Thread(MonitorQueueAndLaunch);
            _launchWaitThread.IsBackground = true;
            _launchWaitThread.Name = "LaunchQueue";
            _launchWaitThread.Start();
        }

        /// <summary>
        /// meant to be run as a thread.  monitors the launch queue and starts clients as necessary
        /// </summary>
        private static void MonitorQueueAndLaunch()
        {
            while (true)
            {
                if (_launchQueue.Count < 1)
                {
                    Thread.Sleep(DELAY_BETWEEN_QUEUE_CHECKS);
                    continue;
                }

                if (!_launchQueue.TryDequeue(out LaunchRequest launch))
                {
                    // this is actually really bad.  should probably log something
                    continue;
                }

                while (Launcher.IsPatching(launch.Subscription.Game))
                {
                    // wait for patching to finish.  should be redundant, but alas...
                    Thread.Sleep(DELAY_BETWEEN_PATCH_CHECKS);
                }

                try
                {
                    Launcher.Launch(launch.Subscription, launch.Shortcut, launch.LauncherConfiguration,
                        launch.GameInformation, launch.ClientFolder, launch.LoginTicket, launch.Language, launch.Use64Bit);

                    Thread.Sleep(DELAY_BETWEEN_LAUNCHES);
                }
                catch (ThreadAbortException)
                {
                    return;
                }
                catch (Exception ex)
                {
                    _log.Debug("Error in LaunchQueue", ex);
                    // in a looping thread, this is just better
                }
            }
        }

        /// <summary>
        /// this really needs to be moved to a mockable interface instead of a single static
        /// </summary>
        public static void LaunchClient(LaunchRequest newLaunch)
        {
            // add to queue
            _launchQueue.Enqueue(newLaunch);
        }
    }
}