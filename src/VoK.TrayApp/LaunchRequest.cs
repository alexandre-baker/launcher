﻿using VoK.ServiceClients;

namespace VoK.TrayApp
{
    public class LaunchRequest
    {
        public Subscription Subscription { get; set; }
        public Shortcut Shortcut { get; set; }
        public LauncherConfiguration LauncherConfiguration { get; set; }
        public GameInformation GameInformation { get; set; }
        public string ClientFolder { get; set; }
        public string LoginTicket { get; set; }
        public string Language { get; set; } = "EN";
        public bool Use64Bit { get; set; } = false;
    }
}