﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using VoK.ServiceClients;
using VoK.TrayApp.Properties;

namespace VoK.TrayApp
{
    public partial class MainForm : Form
    {
        enum NavTab
        {
            Home,
            Accounts,
            Settings
        }

        private NavTab _currentTab;

        private readonly Color _navActive = Color.FromArgb(16, 79, 153); // #104F99
        private readonly Color _navInactive = Color.FromArgb(15, 36, 79); // #???
        private readonly Color _turquoise = Color.FromArgb(51, 252, 254); // #2FD5E7

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        private readonly ToolTip addAccountsToolTip = new ToolTip();

        // default to past time
        private static DateTime _lastPatchCheckTime = new DateTime();

        //[DllImport("user32.dll")]
        //public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        internal static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        internal static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        public bool IsActive { get; private set; }

        private readonly Dictionary<GameId, LauncherConfiguration> _launcherInfo = new Dictionary<GameId, LauncherConfiguration>();
        private readonly Dictionary<GameId, GameInformation> _gameInfo = new Dictionary<GameId, GameInformation>();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MainForm()
        {
            InitializeComponent();
            pAccounts.Dock = DockStyle.Fill;
            pHome.Dock = DockStyle.Fill;
            pSettings.Dock = DockStyle.Fill;
            pUserSettings.Dock = DockStyle.Fill;
            pEditAccount.Dock = DockStyle.Fill;
            pMainMenu.Dock = DockStyle.Fill;
            scAccountDetails.Dock = DockStyle.Fill;
            _currentTab = NavTab.Accounts;
            lblServerStatus.Text = "";

            new ToolTip().SetToolTip(btnAccounts, "Launch & Accounts");
            new ToolTip().SetToolTip(btnHome, "Contact Links");
            new ToolTip().SetToolTip(btnSettings, "Settings");
            new ToolTip().SetToolTip(btnFindDdo, "Browse for Folder Path");
            new ToolTip().SetToolTip(btnFindLotro, "Browse for Folder Path");
            new ToolTip().SetToolTip(btnPlay, "Launch this account's game client with the selected server");
            new ToolTip().SetToolTip(btnDDOPatchNow, "Manually Patch the DDO Client");
            new ToolTip().SetToolTip(btnLOTROPatchNow, "Manually Patch the LOTRO Client");
            new ToolTip().SetToolTip(btnWeb, "Open the Vault of Kundarak Web site");
            new ToolTip().SetToolTip(btnDiscord, "Open the Vault of Kundarak Discord site");
            new ToolTip().SetToolTip(btnGitlab, "Open the Vault of Kundarak Gitlab site");
            new ToolTip().SetToolTip(btnAddShortcut, "Add a new shortcut with the selected server and character name.");

            PopulateUserSettings();
            RenderMenus();
            LoadGameInfo();
            PopulateAccounts();

            // clean up the size, as we design with things way expanded
            // to make them easier to see &  find
            Width = 800;
            Height = 500;

            SendMessage(tbAddShortcutCharacter.Handle, 0x1501, 1, "(character name)");
            SendMessage(tbShortcutDisplayName.Handle, 0x1501, 1, "(display name)");
        }

        private void RenderMenus()
        {
            pHome.Visible = false;
            pAccounts.Visible = false;
            pSettings.Visible = false;
            pUserSettings.Visible = false;
            scAccountDetails.Visible = false;
            btnAccounts.BackColor = _navInactive;
            btnHome.BackColor = _navInactive;
            btnSettings.BackColor = _navInactive;
            btnAddShortcut.Text = "Create Shortcut";

            switch (_currentTab)
            {
                case NavTab.Accounts:
                    // left side
                    pAccounts.Visible = true;
                    btnAccounts.BackColor = _navActive;
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                    if (settings?.StoredAccounts?.Count > 0)
                    {
                        var firstAccount = settings?.StoredAccounts.FirstOrDefault();
                        if (firstAccount?.Subscriptions.Count > 0)
                        {
                            SelectSubscription(firstAccount.Subscriptions[0]);
                        }
                    }

                    break;
                case NavTab.Home:
                    pHome.Visible = true;
                    btnHome.BackColor = _navActive;
                    break;
                case NavTab.Settings:
                    pSettings.Visible = true;
                    btnSettings.BackColor = _navActive;
                    break;
            }
        }

        private void ToggleTrayIcon()
        {
            // change the icon:
            if (Program.TrayIcon != null)
                Program.TrayIcon.ToggleTrayIcon();
        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            _currentTab = NavTab.Accounts;
            RenderMenus();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings.EnableNotifications)
                Program.TrayIcon.PopupNotification("The Vault of Kundarak is still running in the System Tray", "VoK");
            Hide();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings.EnableNotifications)
                Program.TrayIcon.PopupNotification("The Vault of Kundarak is still running in the System Tray", "VoK");
            Hide();
        }

        private static Thread _autoPatchThread = null;

        public static void LaunchPatchThread()
        {
            if (_autoPatchThread != null)
                _autoPatchThread.Abort();

            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            if (settings.CheckForPatchMinutes == null)
                return;

            _autoPatchThread = new Thread(RunPatch);
            _autoPatchThread.IsBackground = true;
            _autoPatchThread.Name = "Patcher";
            _autoPatchThread.Start();
        }

        public async static void RunPatch()
        {
            bool run = true;

            // wait 10 seconds after startup
            Thread.Sleep(10000);

            while (run)
            {
                try
                {
                    // always get a fresh copy of the settings
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                    // check for this first in case they turned it off since we last slept
                    if (settings.CheckForPatchMinutes == null)
                        return;

                    var _checkForPatchMilliseconds = settings.CheckForPatchMinutes.Value * 60000;

                    if (DateTime.UtcNow >= _lastPatchCheckTime.AddMilliseconds(_checkForPatchMilliseconds))
                    {
                        _lastPatchCheckTime = DateTime.UtcNow;

                        if (!string.IsNullOrWhiteSpace(settings.DdoPath) && !Launcher.ProccessExists(GameId.DDO))
                        {
                            if (Program.TrayIcon != null)
                            {
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("Checking for DDO patches...", "VoK Auto Patcher");
                                Application.DoEvents();
                            }

                            var ddoInfo = ServiceClientHost.Instance.GetGameInformation(GameId.DDO);
                            Launcher.PatchClient(settings.DdoPath, GameId.DDO, ddoInfo.PatchServer, null, null);

                            if (Program.TrayIcon != null)
                            {
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("DDO Patch complete.", "VoK Auto Patcher");
                                Application.DoEvents();
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(settings.LotroPath) && !Launcher.ProccessExists(GameId.LOTRO))
                        {
                            if (Program.TrayIcon != null)
                            {
                                Program.TrayIcon.ToggleTrayIcon(true);
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("Checking for LOTRO patches...", "VoK Auto Patcher");
                            }

                            var lotroInfo = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO);
                            Launcher.PatchClient(settings.DdoPath, GameId.LOTRO, lotroInfo.PatchServer, null, null);

                            if (Program.TrayIcon != null)
                            {
                                Program.TrayIcon.ToggleTrayIcon(false);
                                if (settings.EnableNotifications)
                                    Program.TrayIcon.PopupNotification("LOTRO Patch complete", "VoK Auto Patcher");
                            }
                        }
                    }

                    Thread.Sleep(_checkForPatchMilliseconds);
                }
                catch (ThreadAbortException)
                {
                    run = false;
                    throw;
                }
                catch
                {
                    // swallow
                }
            }
        }

        //private void WaitForIconToLoad()
        //{
        //    Thread t = new Thread(() =>
        //    {
        //        while (Program.TrayIcon == null)
        //            Thread.Sleep(500);
        //    });
        //    t.IsBackground = true;
        //    t.Start();
        //}

        private void scHeader_Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, null);
            }
        }

        private void lblTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, null);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pMessages.Dock = DockStyle.Fill;
            pAddAccount.Dock = DockStyle.Fill;
            IsActive = true;
            LaunchPatchThread();
            // WaitForIconToLoad();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
            IsActive = false;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            _currentTab = NavTab.Home;
            RenderMenus();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            _currentTab = NavTab.Settings;
            RenderMenus();
            PopulateUserSettings();
            pUserSettings.Visible = true;
        }

        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            scAccountDetails.Visible = false;
            pAddAccount.Visible = false;
            pEditAccount.Visible = false;
            pAddAccount.Visible = true;
        }

        private void ShowMessage(string messageText)
        {
            lblMessage.Text = messageText;
            lblMessage.Visible = true;
            pMessages.Visible = true;
        }

        private void btnAccountSave_Click(object sender, EventArgs e)
        {
            if (tbAccountUsername?.Text.Length > 0 && tbAccountPassword?.Text.Length > 0)
            {
                if (tbAccountDisplayName.Text.Length <= 0)
                    tbAccountDisplayName.Text = tbAccountUsername.Text;

                // start testing - eventually
                var userProfile = ServiceClientHost.Instance.Authenticate(tbAccountUsername.Text, tbAccountPassword.Text, out string message);

                if (userProfile == null)
                {
                    ShowMessage(message);
                    // check the error and report somehow?
                    return;
                }

                // If the subscriptions come back, pop-up subscription panel with pre-populated drop-downs to select the correct data.
                if (userProfile.Subscriptions.Count > 0)
                {
                    var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                    var existingAccount = settings.StoredAccounts.FirstOrDefault(s => s.Username == tbAccountUsername.Text);

                    if (existingAccount != null)
                        settings.StoredAccounts.Remove(existingAccount);

                    StoredAccount sa = new StoredAccount();
                    sa.DecryptedPassword = tbAccountPassword.Text;
                    sa.Username = tbAccountUsername.Text;
                    sa.DisplayName = tbAccountDisplayName.Text;

                    sa.Subscriptions = userProfile.Subscriptions.Select(s => new Subscription() { DisplayName = s.Description, Game = s.Game, Id = s.Id, Name = s.Description }).ToList();

                    settings.StoredAccounts.Add(sa);
                    SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);

                    tbAccountDisplayName.Text = "";
                    tbAccountPassword.Text = "";
                    tbAccountUsername.Text = "";
                    pAddAccount.Visible = false;

                    PopulateAccounts();
                }
                else
                {
                    // report errors?
                    ShowMessage("No subscriptions found.");
                }
            }
        }

        private void PopulateAccounts()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            pAccounts.Controls.Clear();

            Button addAccount = new Button();
            addAccount.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            addAccount.FlatStyle = FlatStyle.Flat;
            addAccount.FlatAppearance.BorderSize = 0;
            addAccount.Margin = new Padding(0);
            addAccount.Padding = new Padding(0);
            addAccount.Location = new Point(0, 0);
            addAccount.Text = "Add Account";
            addAccount.Font = new Font("Neo Sans", 12, FontStyle.Bold, GraphicsUnit.Point);
            addAccount.Size = new Size(220, 50);
            addAccount.ForeColor = Color.White;
            addAccount.TextAlign = ContentAlignment.MiddleCenter;
            addAccount.Cursor = Cursors.Hand;
            addAccount.UseVisualStyleBackColor = false;
            addAccount.TextImageRelation = TextImageRelation.ImageBeforeText;
            addAccount.Click += btnAddAccount_Click;
            addAccountsToolTip.SetToolTip(addAccount, "Add Account");
            addAccount.Paint += paintBorderInsideButton;
            pAccounts.Controls.Add(addAccount);

            GenerateAccountElements(settings.StoredAccounts);
        }

        private void GenerateAccountElements(List<StoredAccount> accounts)
        {
            int verticalOffset = 47;

            if (accounts == null || accounts.Count < 1)
            {
                Label info = new Label();
                info.Name = "lblInfo";
                info.Text = "No accounts configured.";
                info.Font = new Font("Neo Sans", 10, FontStyle.Bold, GraphicsUnit.Point);
                info.Size = new Size(220, 30);
                info.ForeColor = Color.White;
                info.Location = new Point(5, verticalOffset);
                info.FlatStyle = FlatStyle.Flat;
                info.TextAlign = ContentAlignment.TopLeft;
                info.Margin = new Padding(0);
                pAccounts.Controls.Add(info);
                return;
            }

            foreach (var sa in accounts)
            {
                Button accountButton = new Button();
                accountButton.Name = "btn" + sa.Username;
                accountButton.Text = sa.DisplayName;
                accountButton.Font = new Font("Neo Sans", 10, FontStyle.Bold, GraphicsUnit.Point);
                accountButton.Size = new Size(220 - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth, 30);
                accountButton.ForeColor = Color.White;
                accountButton.Location = new Point(5, verticalOffset);
                accountButton.Click += btnAccount_Click;
                accountButton.Cursor = Cursors.Hand;
                accountButton.FlatStyle = FlatStyle.Flat;
                accountButton.FlatAppearance.BorderSize = 0;
                accountButton.TextAlign = ContentAlignment.MiddleLeft;
                accountButton.Tag = sa;
                accountButton.UseVisualStyleBackColor = false;
                accountButton.Margin = new Padding(0);
                pAccounts.Controls.Add(accountButton);
                verticalOffset += accountButton.Height;

                foreach (var sub in sa.Subscriptions)
                {
                    var subName = sub.DisplayName;
                    // limit character length to 25, to prevent the icon from wrapping because of non-visible text
                    if (subName.Length > 25)
                        subName = subName.Substring(0, 25);

                    Button b = new Button();
                    b.Name = "btn" + sub.Id;
                    b.Text = subName;
                    b.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                    b.Size = new Size(200, 30);
                    b.Cursor = Cursors.Hand;
                    b.FlatStyle = FlatStyle.Flat;
                    b.FlatAppearance.BorderSize = 0;
                    b.ForeColor = Color.White;
                    b.Location = new Point(5, verticalOffset);

                    if (sub.Game == GameId.DDO)
                        b.Image = Resources.ddo_20x20;
                    else
                        b.Image = Resources.lotro_20x20;

                    b.ImageAlign = ContentAlignment.MiddleLeft;
                    b.TextAlign = ContentAlignment.MiddleLeft;
                    b.TextImageRelation = TextImageRelation.ImageBeforeText;
                    b.UseVisualStyleBackColor = false;
                    b.Margin = new Padding(0);
                    b.Click += btnSubscription_Click;
                    b.Tag = sub;

                    pAccounts.Controls.Add(b);
                    verticalOffset += 30;
                }
            }
        }

        private void EditShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                dynamic d = btn.Tag;
                Shortcut sc = d.Shortcut;

                cbMakeDefault.Checked = false;
                cbPlayServers.Text = sc.Server;
                tbAddShortcutCharacter.Text = sc.Character;
                tbShortcutDisplayName.Text = sc.DisplayName;
                btnCancelShortcut.Visible = true;
                btnAddShortcut.Text = "Update Shortcut";
                btnAddShortcut.Tag = btn.Tag;
            }
        }

        private void UpdateShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                dynamic d = btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Sub;

                var itemIndex = sub.Shortcuts.FindIndex(s => s == sc);
                sc.Character = tbAddShortcutCharacter.Text;
                sc.Server = cbPlayServers.Text;
                sc.DisplayName = tbShortcutDisplayName.Text;
                cbMakeDefault.Checked = false;
                cbPlayServers.Text = sub.DefaultServer;

                if (itemIndex > -1)
                    sub.Shortcuts[itemIndex] = sc;
                else
                    // removed?
                    sub.Shortcuts.Add(sc);

            }

            tbAddShortcutCharacter.Text = string.Empty;
            tbShortcutDisplayName.Text = string.Empty;
            btnAddShortcut.Text = "Create Shortcut";
            btnAddShortcut.Tag = null;
            btnCancelShortcut.Visible = false;

            SaveSettings();
            PopulateSubscriptionElements();
        }

        private void DeleteShortcut_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                dynamic d = btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Sub;
                sub.Shortcuts.Remove(sc);
            }

            SaveSettings();
            PopulateSubscriptionElements();
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (sender as Button)?.Tag as StoredAccount;
            SelectAccount(sa);
        }

        private void SelectAccount(StoredAccount sa)
        {
            pEditAccount.Tag = sa;
            tbEditUsername.Text = sa.Username;
            SendMessage(tbEditPassword.Handle, 0x1501, 1, "(enter to change)");
            tbEditDisplayName.Text = sa.DisplayName;

            pEditAccount.Visible = true;
            pAddAccount.Visible = false;
            scAccountDetails.Visible = false;
        }

        private void SelectSubscription(Subscription sub)
        {
            // show main panel
            scAccountDetails.Visible = true;
            pEditAccount.Visible = false;
            pAddAccount.Visible = false;
            scAccountDetails.Tag = sub;
            lblSubscriptionInfo.Text = "Subscription Id: " + sub.Id;
            tbEditSubDisplayName.Text = sub.DisplayName;

            // get the servers for the game
            var gameInfo = ServiceClientHost.Instance.GetGameInformation((GameId)sub.Game);

            cbPlayServers.Items.Clear();

            if (gameInfo != null)
            {
                foreach (var server in gameInfo.Worlds)
                    cbPlayServers.Items.Add(server.Name);
            }
            else
            {
                string[] gameServers = new string[] { };

                switch (sub.Game)
                {
                    case GameId.DDO:
                        if (!string.IsNullOrWhiteSpace(Resources.DDOServers))
                        {
                            gameServers = Resources.DDOServers.Split(new char[] { ',' });
                        }
                        break;
                    case GameId.LOTRO:
                        if (!string.IsNullOrWhiteSpace(Resources.DDOServers))
                        {
                            gameServers = Resources.LOTROServers.Split(new char[] { ',' });
                        }
                        break;
                }

                if (gameServers?.Length > 0)
                {
                    foreach (var server in gameServers)
                        cbPlayServers.Items.Add(server);
                }
                else
                {
                    cbPlayServers.Items.Add(string.Empty);
                }
            }

            if (!string.IsNullOrWhiteSpace(sub.DefaultServer))
                cbPlayServers.SelectedItem = sub.DefaultServer;

            PopulateSubscriptionElements();
        }

        private void PopulateSubscriptionElements()
        {
            // generate UI for the selected sub
            if (!(scAccountDetails.Tag is Subscription))
                return;

            pShortcuts.Controls.Clear();

            Subscription selectedSub = (Subscription)scAccountDetails.Tag;

            // create a label
            Label lbl = new Label();
            lbl.Text = "Shortcuts (Click to Play!)";
            lbl.Font = new Font("Neo Sans", 12, FontStyle.Bold, GraphicsUnit.Point);
            lbl.AutoSize = true;
            lbl.ForeColor = Color.Black;
            pShortcuts.Controls.Add(lbl);

            //pShortcuts.Dock = DockStyle.Fill;

            pShortcuts.Height = 284;
            pShortcuts.Width = 504;

            int yStart = 25;
            int scY_offset = yStart;
            int scX_offset = 5;
            int shortcutWidth = 187;
            int shortcutHeight = 30;
            var shortcuts = selectedSub.Shortcuts.OrderBy(s => s.GetDisplayName());

            foreach (var sc in shortcuts)
            {
                Button shortcutButton = new Button();
                shortcutButton.Text = sc.GetDisplayName();
                shortcutButton.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                shortcutButton.Size = new Size(shortcutWidth, shortcutHeight);
                shortcutButton.Cursor = Cursors.Hand;
                shortcutButton.FlatStyle = FlatStyle.Flat;
                shortcutButton.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                shortcutButton.FlatAppearance.BorderSize = 0;
                shortcutButton.BackColor = _navInactive;
                shortcutButton.ForeColor = _turquoise;
                shortcutButton.Tag = new { Sub = selectedSub, Shortcut = sc };
                shortcutButton.Click += ShortcutButton_Click;

                if ((scY_offset + SystemInformation.HorizontalScrollBarHeight) > pShortcuts.Height)
                {
                    scY_offset = yStart;
                    scX_offset = scX_offset + shortcutButton.Width + 65;
                    if (scX_offset > 400)
                    {
                        pShortcuts.AutoScroll = false;
                        pShortcuts.VerticalScroll.Value = 0;
                        pShortcuts.VerticalScroll.Visible = false;
                        pShortcuts.VerticalScroll.Enabled = false;
                        pShortcuts.AutoScroll = true;
                    }
                }

                shortcutButton.Location = new Point(scX_offset, scY_offset);
                pShortcuts.Controls.Add(shortcutButton);

                // add an edit button
                Button editShortcut = new Button();
                editShortcut.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                editShortcut.Size = new Size(24, 30);
                editShortcut.Margin = new Padding(0, 0, 4, 0);
                editShortcut.Padding = new Padding(0);
                editShortcut.FlatStyle = FlatStyle.Flat;
                editShortcut.Cursor = Cursors.Hand;
                editShortcut.FlatAppearance.BorderSize = 0;
                editShortcut.BackColor = _navInactive;
                editShortcut.ForeColor = _turquoise;
                editShortcut.Location = new Point(shortcutButton.Size.Width + shortcutButton.Location.X + 4, scY_offset);
                editShortcut.Image = Resources.box_edit_16;
                editShortcut.ImageAlign = ContentAlignment.MiddleCenter;
                editShortcut.Tag = new { Sub = selectedSub, Shortcut = sc };
                editShortcut.Click += EditShortcut_Click;

                pShortcuts.Controls.Add(editShortcut);

                // add a delete button too
                Button deleteShortcut = new Button();
                deleteShortcut.Font = new Font("Neo Sans", 10, FontStyle.Regular, GraphicsUnit.Point);
                deleteShortcut.Size = new Size(24, 30);
                deleteShortcut.Margin = new Padding(0, 0, 4, 0);
                deleteShortcut.Padding = new Padding(0, 3, 3, 0);
                deleteShortcut.FlatStyle = FlatStyle.Flat;
                deleteShortcut.Cursor = Cursors.Hand;
                deleteShortcut.FlatAppearance.BorderSize = 0;
                deleteShortcut.BackColor = _navInactive;
                deleteShortcut.ForeColor = _turquoise;
                deleteShortcut.Location = new Point(editShortcut.Size.Width + editShortcut.Location.X + 4, scY_offset);
                deleteShortcut.Tag = new { Sub = selectedSub, Shortcut = sc };
                deleteShortcut.Image = Resources.trash_can_16;
                deleteShortcut.ImageAlign = ContentAlignment.MiddleCenter;
                deleteShortcut.Click += DeleteShortcut_Click;

                pShortcuts.Controls.Add(deleteShortcut);

                scY_offset += shortcutButton.Height + 5;
            }
        }

        private async void ShortcutButton_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                dynamic d = btn.Tag;
                Shortcut sc = d.Shortcut;
                Subscription sub = d.Sub;

                Thread t = new Thread(() => Play(sub, sc));
                t.IsBackground = true;
                t.Name = "ShortcutPlay";
                t.Start();
            }
        }

        private void btnSubscription_Click(object sender, EventArgs e)
        {
            if ((sender as Button).Tag is Subscription sub)
                SelectSubscription(sub);
        }

        private void ResetAccountAddFields()
        {
            tbAccountDisplayName.Text = "";
            tbAccountPassword.Text = "";
            tbAccountUsername.Text = "";
        }

        private void btnAccountCancel_Click(object sender, EventArgs e)
        {
            ResetAccountAddFields();
            pAddAccount.Visible = false;
        }

        private void tbAccountUsername_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbAccountDisplayName.Text))
                tbAccountDisplayName.Text = tbAccountUsername.Text;
        }

        private void btnMessagesOkay_Click(object sender, EventArgs e)
        {
            pMessages.Visible = false;
        }

        private void btnRefreshStatus_Click(object sender, EventArgs e)
        {
            btnRefreshStatus.Enabled = false;
            lblServerStatus.Text = "Loading...";
            LoadGameInfo();
            btnRefreshStatus.Enabled = true;
        }

        private void LoadGameInfo()
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                sb.AppendLine("DDO");
                var ddoInfo = ServiceClientHost.Instance.GetGameInformation(GameId.DDO, true);
                _gameInfo[GameId.DDO] = ddoInfo;
                _launcherInfo[GameId.DDO] = ServiceClientHost.Instance.GetLauncherConfiguration(ddoInfo);
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            sb.AppendLine();

            try
            {
                sb.AppendLine("LOTRO");
                var lotroInfo = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO, true);
                _gameInfo[GameId.LOTRO] = lotroInfo;
                _launcherInfo[GameId.LOTRO] = ServiceClientHost.Instance.GetLauncherConfiguration(lotroInfo);
            }
            catch (Exception ex)
            {
                sb.AppendLine("Error - " + ex.Message);
            }

            lblServerStatus.Text = sb.ToString();
        }

        private async void btnPlay_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cbPlayServers.Text))
                return;

            Subscription sub = (Subscription)scAccountDetails.Tag;
            Shortcut sc = new Shortcut() { Server = cbPlayServers.Text, Character = tbAddShortcutCharacter.Text };

            if (cbMakeDefault.Checked)
            {
                sub.DefaultServer = cbPlayServers.Text;
                SaveSettings();
            }

            Thread t = new Thread(() => Play(sub, sc));
            t.IsBackground = true;
            t.Name = "DirectPlay";
            t.Start();
        }

        private void btnRemoveAccount_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (StoredAccount)pEditAccount.Tag;
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

            // this is wishful thinking and prone to future bugs
            settings.StoredAccounts.Remove(sa);

            pEditAccount.Visible = false;

            PopulateAccounts();
        }

        private void btnEditAccountCancel_Click(object sender, EventArgs e)
        {
            // don't leave passwords partially entered hanging around in memory
            tbEditPassword.Text = "";

            pEditAccount.Visible = false;
        }

        private void btnEditAccountSave_Click(object sender, EventArgs e)
        {
            StoredAccount sa = (StoredAccount)pEditAccount.Tag;

            if (!string.IsNullOrWhiteSpace(tbEditPassword.Text))
                sa.DecryptedPassword = tbEditPassword.Text;

            sa.DisplayName = tbEditDisplayName.Text;

            // this SHOULD be a pointer to the root object that this StoredAccount belongs to, but
            // there are all sorts of risks with assuming that.  /sigh.
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            pEditAccount.Visible = false;

            // call repopulate to force changes to display name to propagate
            PopulateAccounts();
        }

        private void btnEditSubSave_Click(object sender, EventArgs e)
        {
            Subscription sub = (Subscription)scAccountDetails.Tag;

            sub.DisplayName = tbEditSubDisplayName.Text;

            if (cbMakeDefault.Checked && cbPlayServers?.Text.Length > 0)
            {
                sub.DefaultServer = cbPlayServers.Text;
                cbMakeDefault.Checked = false;
            }

            SaveSettings();
            PopulateAccounts();
            SelectSubscription(sub);
        }

        private void tbnAddUpdateShortcut_Click(object sender, EventArgs e)
        {
            // Update existing shortcut
            if (btnAddShortcut.Text.Contains("Update"))
            {
                UpdateShortcut_Click(sender, e);
                return;
            }

            Subscription sub = (Subscription)scAccountDetails.Tag;

            if (string.IsNullOrWhiteSpace(cbPlayServers.Text))
                return;

            Shortcut sc = new Shortcut();
            sc.Server = cbPlayServers.Text;
            sc.Character = tbAddShortcutCharacter.Text;
            sc.DisplayName = tbShortcutDisplayName.Text;

            sub.Shortcuts.Add(sc);

            if (cbMakeDefault.Checked)
            {
                sub.DefaultServer = cbPlayServers.Text;
            }

            SaveSettings();

            tbAddShortcutCharacter.Text = "";
            tbShortcutDisplayName.Text = "";
            PopulateAccounts();
            SelectSubscription(sub);

            // restore the selected server
            cbPlayServers.Text = sc.Server;
        }

        private string GetClientGamePath(GameId gameId, Settings settings, LauncherConfiguration launcherInfo)
        {
            string clientPath = string.Empty;

            // Validate the GameId
            if (Enum.IsDefined(typeof(GameId), gameId))
            {
                // if Game is valid, get path from settings:
                if (gameId == GameId.DDO)
                    clientPath = settings?.DdoPath != string.Empty ? settings?.DdoPath : string.Empty;
                else if (gameId == GameId.LOTRO)
                    clientPath = settings?.LotroPath != string.Empty ? settings?.LotroPath : string.Empty;

                // Validate the Path
                if (string.IsNullOrEmpty(clientPath))
                {
                    // bail out, we don't have a folder set to launch from.
                    MessageBox.Show($"Your settings path is empty, please set the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }
                if (!Directory.Exists(clientPath))
                {
                    // bail out, The folder is incorrect.
                    MessageBox.Show($"The path in your settings is invalid or does not exist, please change the path to your {gameId} game client.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    return null;
                }

                var clientExePath = Path.Combine(clientPath, launcherInfo.Settings["GameClient.WIN32.Filename"]);

                // check if the client executable exists
                if (!File.Exists(clientExePath))
                {
                    //MessageBox.Show("There was an issue locating the client executable.", $"Update the path to the {gameId} game client in the settings menu.", MessageBoxButtons.OK);
                    //return null;
                }

                var clientPatchDllPath = Path.Combine(clientPath, @"patchclient.dll");

                // check if the patchclient.dll file exists
                if (!File.Exists(clientPatchDllPath))
                {
                    MessageBox.Show("There was an issue locating the client patcher.", $"Cannot Patch game {gameId}.", MessageBoxButtons.OK);
                    // disable auto-patching?
                    // return null;
                    settings.PatchBeforeLaunch = false;
                }
            }
            else
            {
                // Invalid GameId // bail out, The folder is incorrect.
                MessageBox.Show("Could not determine a valid Game to launch.", "Please update your subscription information, under your account.", MessageBoxButtons.OK);
                return null;
            }
            return clientPath;
        }

        public void Play(Subscription sub, Shortcut shortcut)
        {
            try
            {
                if (sub == null || shortcut == null)
                {
                    // fix password/account
                    MessageBox.Show("Please re-enter your account information, for this subscription.",
                        "Please update your subscription information, under your account.", MessageBoxButtons.OK);
                    return;
                }

                // go find the account for this sub
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();

                if (settings.MinimizeBeforeLaunch)
                    Hide_MainForm();

                var account = settings.StoredAccounts.FirstOrDefault(s => s.Subscriptions.Contains(sub));

                if (string.IsNullOrWhiteSpace(account?.DecryptedPassword))
                {
                    MessageBox.Show($"Please re-enter your account information, for this account {account?.Username}.",
                        "Please check your account or the password, in the settings menu.", MessageBoxButtons.OK);
                    return;
                }

                LaunchRequest newLaunch = new LaunchRequest() { Subscription = sub, Shortcut = shortcut };

                newLaunch.LoginTicket = ServiceClientHost.Instance.Authenticate(account.Username, account.DecryptedPassword, out string message).Ticket;

                // capture errors:
                if (message?.Length > 0)
                {
                    MessageBox.Show($"Please check your account or the password, in the settings menu.\n\nThe server returned a possible error message: {message}", $"Error authenticating {account.Username}.", MessageBoxButtons.OK);
                    return;
                }

                if (!_launcherInfo.ContainsKey(sub.Game))
                {
                    MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", $"ERROR: Could not connect too the {sub.Game} servers.", MessageBoxButtons.OK);
                    return;
                }

                newLaunch.LauncherConfiguration = _launcherInfo[sub.Game];
                newLaunch.GameInformation = _gameInfo[sub.Game];
                // must come after settings:
                newLaunch.ClientFolder = GetClientGamePath(sub.Game, settings, newLaunch.LauncherConfiguration);
                newLaunch.Use64Bit = settings.Use64BitClients;

                // This must be valid, to launch the game. Errors are show in a MessageBox, from within GetClientGamePath
                if (newLaunch.ClientFolder == null)
                    return;

                if (settings.PatchBeforeLaunch)
                {
                    if (!Launcher.IsPatching(newLaunch.GameInformation.Game) &&
                        !Launcher.ProccessExists(newLaunch.GameInformation.Game))
                    {
                        if (Program.TrayIcon != null)
                        {
                            Program.TrayIcon.ToggleTrayIcon();
                            if (settings.EnableNotifications)
                                Program.TrayIcon.PopupNotification($"Patching {newLaunch.GameInformation.Game}",
                                    "VoK Auto Patcher");
                        }

                        Launcher.PatchClient(newLaunch.ClientFolder, newLaunch.GameInformation.Game,
                            newLaunch.GameInformation.PatchServer, null, null);

                        if (Program.TrayIcon != null)
                        {
                            Program.TrayIcon.ToggleTrayIcon();
                            if (settings.EnableNotifications)
                                Program.TrayIcon.PopupNotification($"Done patching {newLaunch.GameInformation.Game}",
                                    "VoK Auto Patcher");
                        }
                    }
                }

                newLaunch.Language = "EN";

                LaunchQueue.LaunchClient(newLaunch);
            }
            catch (Exception ex)
            {
                _log.Error("Error in MainForm.Play", ex);
            }
        }

        private void SaveSettings()
        {
            // this SHOULD be a pointer to the root object that this StoredAccount belongs to, but
            // there are all sorts of risks with assuming that.  /sigh.
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            settings.DdoPath = txtBxDDOPath?.Text;
            settings.LotroPath = txtBxLOTROPath?.Text;
            settings.PatchBeforeLaunch = cbAutoPatch.Checked;
            settings.EnableNotifications = cbEnablePatchNotifications.Checked;
            settings.MinimizeBeforeLaunch = cbMinimizeOnLaunch.Checked;
            settings.CheckForPatchMinutes = null;
            settings.Use64BitClients = cb64bit.Checked;

            // these are disabled for now
            //settings.EnableLogging = cbEnableLogging.Checked;
            //settings.KeepRawData = cbKeepRawData.Checked;

            SettingsFactoryHost.DefaultSettingsFactory.SaveSettings(settings);
            LaunchPatchThread();
        }

        private void btnUserSettingsSave_Click(object sender, EventArgs e)
        {
            SaveSettings();
            PopulateUserSettings();
        }

        private void PopulateUserSettings()
        {
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            if (settings != null)
            {
                if (settings.DdoPath?.Length > 0)
                    txtBxDDOPath.Text = settings.DdoPath;
                if (settings.LotroPath?.Length > 0)
                    txtBxLOTROPath.Text = settings.LotroPath;
                cbAutoPatch.Checked = settings.PatchBeforeLaunch;
                cbEnablePatchNotifications.Checked = settings.EnableNotifications;
                cbKeepRawData.Checked = settings.KeepRawData;
                cbEnableLogging.Checked = settings.EnableLogging;
                cbMinimizeOnLaunch.Checked = settings.MinimizeBeforeLaunch;
                cb64bit.Checked = settings.Use64BitClients;
            }
        }

        private void GoToWebsite(string siteUrl)
        {
            try
            {
                Process.Start(siteUrl);
            }
            catch
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    siteUrl = siteUrl.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {siteUrl}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", siteUrl);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", siteUrl);
                }
                else
                {
                    throw;
                }
            }
        }

        private void btnWeb_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.WebsiteUrl);
        }

        private void btnDiscord_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.DiscordUrl);
        }

        private void btnGitlab_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.GitlabUrl);
        }

        private async void btnDDOPatchNow_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_gameInfo.ContainsKey(GameId.DDO))
                {
                    var temp = ServiceClientHost.Instance.GetGameInformation(GameId.DDO);

                    if (temp == null)
                    {
                        // most likely down for maintenance or internet connectivity issues
                        MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", "ERROR: Could not patch the DDO game client files.", MessageBoxButtons.OK);
                        return;
                    }

                    _gameInfo.Add(GameId.DDO, temp);
                }

                var gameInfo = _gameInfo[GameId.DDO];

                if (gameInfo == null)
                    LoadGameInfo();

                gameInfo = _gameInfo[GameId.DDO];
                if (gameInfo == null)
                {
                    MessageBox.Show("Unable to get game information from SSG servers.  The game is most likely down for some reason or another.");
                    return;
                }

                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                var launcherInfo = ServiceClientHost.Instance.GetLauncherConfiguration(gameInfo);
                if (settings != null && launcherInfo != null)
                {
                    var clientPath = GetClientGamePath(gameInfo.Game, settings, launcherInfo);
                    if (!string.IsNullOrWhiteSpace(clientPath))
                    {
                        ToggleTrayIcon();
                        using (TextOutputWindow window = new TextOutputWindow(true))
                        {

                            window.Show();
                            Launcher.PatchClient(clientPath, gameInfo.Game, gameInfo.PatchServer, window.AppendPatchChar, window.EnableButtons);

                            while (window.DialogResult != DialogResult.OK)
                            {
                                await Task.Delay(500);
                            }
                        }

                        ToggleTrayIcon();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was a problem patching the client: " + ex.ToString());
            }
        }

        private async void btnLOTROPatchNow_Click(object sender, EventArgs e)
        {
            if (!_gameInfo.ContainsKey(GameId.LOTRO))
            {
                var temp = ServiceClientHost.Instance.GetGameInformation(GameId.LOTRO);

                if (temp == null)
                {
                    // most likely down for maintenance or internet connectivity issues
                    MessageBox.Show("Most likely causes: The game servers are down for maintenance or you are experiencing internet connectivity issues.", "ERROR: Could not patch the LOTRO game client files.", MessageBoxButtons.OK);
                    return;
                }

                _gameInfo.Add(GameId.LOTRO, temp);
            }

            var gameInfo = _gameInfo[GameId.LOTRO];
            var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
            var launcherInfo = ServiceClientHost.Instance.GetLauncherConfiguration(gameInfo);
            if (settings != null && launcherInfo != null)
            {
                var clientPath = GetClientGamePath(gameInfo.Game, settings, launcherInfo);
                if (!string.IsNullOrWhiteSpace(clientPath))
                {
                    using (TextOutputWindow window = new TextOutputWindow(true))
                    {
                        ToggleTrayIcon();
                        window.Show();
                        Launcher.PatchClient(clientPath, gameInfo.Game, gameInfo.PatchServer, window.AppendPatchChar, window.EnableButtons);

                        while (window.DialogResult != DialogResult.OK)
                        {
                            await Task.Delay(500);
                        }

                        ToggleTrayIcon();
                    }
                }
            }
        }
        private string AttemptLocatingLOTROPath()
        {
            //C:\Program Files (x86)\StandingStoneGames\The Lord of the Rings Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

            string testPath = Path.Combine(path, "StandingStoneGames");

            if (Directory.Exists(testPath))
            {
                var lotroPath = Path.Combine(testPath, "The Lord of the Rings Online");
                if (Directory.Exists(lotroPath))
                {
                    return lotroPath;
                }
            }

            return path;
        }

        private string AttemptLocatingDDOPath()
        {
            //C:\Program Files (x86)\StandingStoneGames\Dungeons & Dragons Online
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);

            string testPath = Path.Combine(path, "StandingStoneGames");

            if (Directory.Exists(testPath))
            {
                var ddoPath = Path.Combine(testPath, "Dungeons & Dragons Online");
                if (Directory.Exists(ddoPath))
                {
                    return ddoPath;
                }
            }

            return path;
        }

        private void btnFindDdo_Click(object sender, EventArgs e)
        {
            var startingFolder = txtBxDDOPath.Text.Length > 0 ? txtBxDDOPath.Text : AttemptLocatingDDOPath();

            using (var browse = new FolderBrowserDialog() { SelectedPath = startingFolder })
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                if (settings == null) return;

                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.SelectedPath))
                {
                    var selectedFolder = Path.GetFullPath(browse.SelectedPath);
                    if (settings.LotroPath == selectedFolder) return;
                    settings.DdoPath = selectedFolder;
                    txtBxDDOPath.Text = selectedFolder;
                    SaveSettings();
                }
            }
        }

        private void btnFindLotro_Click(object sender, EventArgs e)
        {
            var startingFolder = txtBxLOTROPath.Text.Length > 0 ? txtBxLOTROPath.Text : AttemptLocatingLOTROPath();

            using (var browse = new FolderBrowserDialog() { SelectedPath = startingFolder })
            {
                var settings = SettingsFactoryHost.DefaultSettingsFactory.GetCurrentSettings();
                if (settings == null) return;

                DialogResult result = browse.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(browse.SelectedPath))
                {
                    var selectedFolder = Path.GetFullPath(browse.SelectedPath);
                    if (settings.LotroPath == selectedFolder) return;
                    settings.LotroPath = selectedFolder;
                    txtBxLOTROPath.Text = selectedFolder;
                    SaveSettings();
                }
            }
        }

        private void btnWikiPage_Click(object sender, EventArgs e)
        {
            GoToWebsite(Resources.WikiUrl);
        }

        public void Hide_MainForm()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    Hide();
                }));
            } else
                Hide();
        }

        /// <summary>
        /// Adds a "turquoise" "border" .... inside of a button ...
        /// </summary>
        private void paintBorderInsideButton(object sender, PaintEventArgs e)
        {
            int borderSize = 2;
            ButtonBorderStyle buttonBorderStyle = ButtonBorderStyle.Solid;
            Rectangle borderRectangle = new Rectangle(e.ClipRectangle.Location, e.ClipRectangle.Size);
            borderRectangle.Inflate(-10, -10);
            Color borderColor = Color.FromArgb(255, 51, 252, 254);
            ControlPaint.DrawBorder(e.Graphics, borderRectangle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle, borderColor, borderSize, buttonBorderStyle);
        }

        private void btnCancelShortcut_Click(object sender, EventArgs e)
        {
            btnAddShortcut.Text = "Create Shortcut";
            btnAddShortcut.Tag = null;
            tbAddShortcutCharacter.Text = string.Empty;
            tbShortcutDisplayName.Text = string.Empty;
            btnCancelShortcut.Visible = false;
        }
    }
}
