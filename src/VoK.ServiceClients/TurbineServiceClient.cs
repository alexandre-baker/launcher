﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VoK.ServiceClients
{
    public class TurbineServiceClient : ITurbineServiceClient
    {
        private ISettingsFactory _settingsFactory;

        /// <summary>
        /// while labeled a cache, this stuff only changes if they change
        /// the order of default worlds.  it's load-once data.
        /// </summary>
        private Dictionary<GameId, GameInformation> _gameInfoCache = new Dictionary<GameId, GameInformation>();

        public TurbineServiceClient(ISettingsFactory settingsFactory)
        {
            _settingsFactory = settingsFactory;
        }

        public UserSession Authenticate(string username, string password, out string message)
        {
            Settings settings = _settingsFactory.GetCurrentSettings();

            // more xml.  oh stupid soap envelopes
            string soapAction = "http://www.turbine.com/SE/GLS/LoginAccount";
            string contentType = "text/xml; charset=utf-8";
            string body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><LoginAccount xmlns=\"http://www.turbine.com/SE/GLS\"><username>" + username + "</username><password>" + password + "</password></LoginAccount></soap:Body></soap:Envelope>";
            message = null;

            RestClient client = new RestClient(settings.AuthServiceUrl);
            client.FollowRedirects = false;
            RestRequest req = new RestRequest(Method.POST);
            req.AddHeader("SOAPAction", soapAction);
            req.AddHeader("Content-Type", contentType);
            req.Parameters.Add(new Parameter("no-name", body, ParameterType.RequestBody));

            var res = client.Execute(req);

            // validate the return here rather than toss exceptions
            switch (res.StatusCode)
            {
                // Unknown
                case 0x00:

                    if (res?.ResponseStatus == ResponseStatus.TimedOut)
                    {
                        message = "Unknown error - a network timeout has occurred.";
                        return null;
                    }

                    break;

                // Redirection Found:
                case HttpStatusCode.Found:

                    message = "The servers are down; check for server maintenance.";
                    return null;

                case HttpStatusCode.InternalServerError:
                    // this is a general catch-all code in soap land for just about anything that goes wrong.
                    if (string.IsNullOrWhiteSpace(res.Content))
                    {
                        message = "Unknown error - no data provided.";
                        return null;
                    }

                    // generic auth failure
                    if (res.Content.Contains("No Subscriber Formal Entity was found"))
                    {
                        message = "Username and/or Password incorrect.";
                        return null;
                    }

                    // attempt to parse the error message
                    dynamic error = DynamicXml.Parse(res.Content);
                    var faultString = error?.Body?.Fault?.faultstring;
                    var faultCode = error?.Body?.Fault?.faultcode;
                    message = $"{faultCode}: {faultString}";
                    return null;

                case HttpStatusCode.OK:
                    {
                        dynamic userProfile = DynamicXml.Parse(res.Content);
                        dynamic result = userProfile?.Body?.LoginAccountResponse?.LoginAccountResult;

                        if (result == null)
                            return null;

                        var subs = result.Subscriptions.GameSubscription;
                        List<GameSubscription> gameSubs = new List<GameSubscription>();

                        if (subs is IEnumerable<object>)
                        {
                            var dynamicSubs = (subs as IEnumerable<object>).Select(s => s as dynamic).ToList();
                            foreach (var dynamicSub in dynamicSubs)
                            {
                                var parsedSub = GameSubscription.ParseFromDynamicServiceResponse(dynamicSub);
                                if (parsedSub != null)
                                    gameSubs.Add(parsedSub);
                            }
                        }
                        else
                        {
                            // subs is just 1
                            var dynamicSub = subs as dynamic;
                            var parsedSub = GameSubscription.ParseFromDynamicServiceResponse(dynamicSub);
                            if (parsedSub != null)
                                gameSubs.Add(parsedSub);
                        }

                        string ticket = result.Ticket;

                        UserSession up = new UserSession(gameSubs, ticket, username);
                        return up;
                    }
            }

            message = "Unhandled http status code " + res.StatusCode;
            return null;
        }

        public LauncherConfiguration GetLauncherConfiguration(GameInformation serviceInformation)
        {
            // launcher stuff is just an HTTP get.
            RestClient client = new RestClient(serviceInformation.LauncherConfigurationServer);
            RestRequest restRequest = new RestRequest(Method.GET);
            var restResponse = client.Execute(restRequest);

            if (restResponse.StatusCode != HttpStatusCode.OK)
                return null;

            LauncherConfiguration lc = new LauncherConfiguration();
            ConfigXmlDocument doc = new ConfigXmlDocument();
            doc.Load(serviceInformation.LauncherConfigurationServer);
            foreach (XmlNode node in doc["configuration"]["appSettings"].ChildNodes)
                if (node.Attributes != null)
                {
                    if (!lc.Settings.ContainsKey(node.Attributes["key"].Value))
                        lc.Settings.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
                    // There is no good way right now for duplicate keys
                    else
                        lc.Settings.Add(node.Attributes["key"].Value + "Secondary", node.Attributes["value"].Value);
                }

            return lc;
        }

        public GameInformation GetGameInformation(GameId game, bool reloadCache)
        {
            UpdateServiceInformationCache(game);

            if (_gameInfoCache.ContainsKey(game))
                return _gameInfoCache[game];

            return null;
        }

        public GameInformation GetGameInformation(GameId game)
        {
            if (!_gameInfoCache.ContainsKey(game))
                UpdateServiceInformationCache(game);

            if (_gameInfoCache.ContainsKey(game))
                return _gameInfoCache[game];

            return null;
        }

        private void UpdateServiceInformationCache(GameId game)
        {
            Settings settings = _settingsFactory.GetCurrentSettings();
            string url = null;

            switch (game)
            {
                case GameId.DDO:
                    url = settings.DdoServiceInfoUrl;
                    break;
                case GameId.LOTRO:
                    url = settings.LotroServiceInfoUrl;
                    break;
                default:
                    throw new NotImplementedException();
            }

            // ugh, xml is evil.  why not just "{ "game": "DDO" }"?
            string body = "<?xml version=\"1.0\" encoding=\"utf - 8\"?><soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><GetDatacenters xmlns=\"http://www.turbine.com/SE/GLS\"><game>" + game.ToString() + "</game></GetDatacenters></soap:Body></soap:Envelope>";

            RestClient client = new RestClient(url);
            client.FollowRedirects = false;
            RestRequest req = new RestRequest(Method.POST);
            req.AddHeader("SOAPAction", "\"http://www.turbine.com/SE/GLS/GetDatacenters\"");
            req.AddHeader("Content-Type", "text/xml; charset=utf-8");
            req.AddHeader("Connection", "Keep-Alive");
            req.Parameters.Add(new Parameter("no-name", body, ParameterType.RequestBody));

            var res = client.Execute(req);

            if (res.StatusCode != HttpStatusCode.OK)
                return;
            
            dynamic dynGameInfo = DynamicXml.Parse(res.Content);

            var dataCenter = dynGameInfo?.Body?.GetDatacentersResponse?.GetDatacentersResult?.Datacenter;
            string name = dataCenter.Name;
            List<GameWorld> worlds = new List<GameWorld>();

            foreach(var world in dataCenter.Worlds.World)
            {
                GameWorld gw = new GameWorld(world.Name, world.LoginServerUrl, world.ChatServerUrl, world.StatusServerUrl, int.Parse(world.Order), world.Language);
                worlds.Add(gw);
            }

            GameInformation gi = new GameInformation(game, worlds, dataCenter.AuthServer, dataCenter.PatchServer, dataCenter.LauncherConfigurationServer);

            _gameInfoCache[game] = gi;
        }
    }
}
