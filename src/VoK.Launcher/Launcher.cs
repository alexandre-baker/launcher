﻿using log4net;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using VoK.ServiceClients;

namespace VoK
{
    public class Launcher
    {
        private static Dictionary<GameId, bool> _patchingDict { get; } = new Dictionary<GameId, bool>();

        private static Dictionary<GameId, object> _patchingMutexes { get; } = new Dictionary<GameId, object>();

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static Launcher()
        {
            // prepopulate the dictionaries
            foreach (GameId game in Enum.GetValues(typeof(GameId)))
            {
                _patchingMutexes.Add(game, new object());
                _patchingDict.Add(game, false);
            }
        }

        /// <summary>
        /// total ripoff of Mogwai's code here, with permission.  he said it's MIT licensed in any case.
        /// </summary>
        public static void Launch(Subscription sub, Shortcut shortcut, LauncherConfiguration launcherData, GameInformation gameInfo, string clientFolder, string loginTicket, string language, bool use64bit = false)
        {
            if (IsPatching(sub.Game))
            {
                // abort
                return;
            }

            GameWorld world = gameInfo.Worlds.First(w => w.Name == shortcut.Server);
            XmlDocument worldStatus = new XmlDocument();
            worldStatus.Load(world.StatusServerUrl);

            // get a login ticket
            string loginTicketUrl = launcherData.Settings["WorldQueue.LoginQueue.URL"];
            string loginTicketParams = launcherData.Settings["WorldQueue.TakeANumber.Parameters"];
            XmlElement status = worldStatus["Status"];
            XmlElement queueUrls = status["queueurls"];
            string queueUrlsContent = queueUrls.InnerText;
            loginTicketParams = string.Format(loginTicketParams,
                sub.Id,
                HttpUtility.UrlEncode(loginTicket),
                queueUrlsContent.Substring(0, queueUrlsContent.IndexOf(";")));

            if (!string.IsNullOrEmpty(queueUrlsContent) && queueUrlsContent != ";")
            {
                string loginTicketResult;
                try
                {
                    loginTicketResult = HttpPost(loginTicketUrl, loginTicketParams);
                }
                catch (Exception ex)
                {
                    Exception newEx = new Exception("Error getting login ticket.", ex);
                    throw newEx;
                }

                XmlDocument loginTicketDoc = new XmlDocument();
                loginTicketDoc.LoadXml(loginTicketResult);

                var hresult = loginTicketDoc["Result"]["HResult"];
                if (hresult != null && hresult.InnerText != "0x00000000")
                {
                    // login queue failed.
                    _log.Warn("unable to access login queue.  hresult: " + hresult.ToString());
                    return;
                }

                long queueNumber = Convert.ToInt64(loginTicketDoc["Result"]["QueueNumber"].InnerText.Substring(2), 16);
                long nowServing = Convert.ToInt64(loginTicketDoc["Result"]["NowServingNumber"].InnerText.Substring(2), 16);

                while (nowServing < queueNumber)
                {
                    Thread.Sleep(1000);
                    nowServing = GetNowServingNumber(world.StatusServerUrl);
                }
            }

            string loginServers = worldStatus["Status"]["loginservers"].InnerText;

            string template = launcherData.Settings["GameClient.WIN32.ArgTemplate"];
            string authServerUrl = launcherData.Settings["GameClient.Arg.authserverurl"];
            string supportUrl = launcherData.Settings["GameClient.Arg.supporturl"];
            string bugUrl = launcherData.Settings["GameClient.Arg.bugurl"];
            string ticketLifetime = launcherData.Settings["GameClient.Arg.glsticketlifetime"];
            string serviceUrl = launcherData.Settings["GameClient.Arg.supportserviceurl"];

            string clientParams = template.Replace("{SUBSCRIPTION}", sub.Id);
            clientParams = clientParams.Replace("{LOGIN}", loginServers.Substring(0, loginServers.IndexOf(';')));
            clientParams = clientParams.Replace("{GLS}", loginTicket);
            clientParams = clientParams.Replace("{CHAT}", world.ChatServerUrl);
            clientParams = clientParams.Replace("{LANG}", language);
            clientParams = clientParams.Replace("{AUTHSERVERURL}", authServerUrl);
            clientParams = clientParams.Replace("{GLSTICKETLIFETIME}", ticketLifetime);
            clientParams = clientParams.Replace("{SUPPORTURL}", supportUrl);
            clientParams = clientParams.Replace("{SUPPORTSERVICEURL}", serviceUrl);
            clientParams = clientParams.Replace("{BUGURL}", bugUrl);

            if (!string.IsNullOrWhiteSpace(shortcut.Character))
                clientParams += " -u " + shortcut.Character;

            string clientFilename = launcherData.Settings["GameClient.WIN32.Filename"];

            if (use64bit)
                clientFilename = "x64\\" + launcherData.Settings["GameClient.WIN64.Filename"];

            Directory.SetCurrentDirectory(clientFolder);

            Process client = new Process();
            client.StartInfo.FileName = clientFilename;
            client.StartInfo.Arguments = clientParams;
            client.Start();
        }

        public static bool ProccessExists(GameId gameId)
        {
            string procName = ProcessHelper.GetProcessName(gameId);
            var running = Process.GetProcessesByName(procName);
            if (running?.Length > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// thread-blocking method that will ensure the game is patched.  spawns a new process to do the patching if one isn't running.
        /// </summary>
        public static void PatchClient(string clientPath, GameId gameId, string patchServer, Action<char> stdOutCallback, Action finishedProcessingCallback)
        {
            if (IsPatching(gameId))
            {
                // wait for it to finish, then return
                while (IsPatching(gameId))
                    Thread.Sleep(500);

                return;
            }

            // lock the mutex
            lock (_patchingMutexes[gameId])
            {
                _patchingDict[gameId] = true;
                _log.Debug($"Patching starting for {gameId} at {clientPath}...");

                try
                {
                    Directory.SetCurrentDirectory(clientPath);
                    string sysFolder = Environment.GetFolderPath(Environment.SpecialFolder.System);
                    string dllPath = Path.Combine(clientPath, "patchclient.dll");
                    string runDllPath = Path.Combine(sysFolder, "rundll32.exe");

                    bool highRes = File.Exists(Path.Combine(clientPath, "client_highres.dat"));

                    // check for currently running processes - we can't patch if the game is running

                    if (ProccessExists(gameId))
                    {
                        string message = "Cannot Patch - client is currently running.";
                        message.ToCharArray().ToList().ForEach(c => stdOutCallback?.Invoke(c));
                        finishedProcessingCallback?.Invoke();
                        _patchingDict[gameId] = false;
                        return;
                    }

                    // Set a time-out value of one hour
                    int timeOut = 1000 * 60 * 60;

                    // Create a new process info structure.
                    ProcessStartInfo pInfo = new ProcessStartInfo();

                    // Set file name to open.
                    pInfo.FileName = sysFolder + @"\rundll32.exe";
                    // Patch(int, int, string, int);
                    string patchString = patchServer + " --productcode " + Enum.GetName(typeof(GameId), gameId) + " --language English";

                    if (highRes)
                        patchString += " --highres";

                    pInfo.Arguments = $"\"{dllPath}\",Patch 0 0 {patchString}";

                    pInfo.UseShellExecute = false;
                    pInfo.RedirectStandardOutput = true;
                    pInfo.RedirectStandardError = true;

                    // Start the process.
                    Process p = Process.Start(pInfo);

                    int charCode;

                    while (!p.StandardOutput.EndOfStream && stdOutCallback != null)
                    {
                        charCode = p.StandardOutput.Read();
                        stdOutCallback((char)charCode);
                    }

                    // Wait for the process to exit or time out.
                    p.WaitForExit(timeOut);

                    // Check to see if the process is still running.
                    if (p.HasExited == false)
                    {
                        // Process is still running.
                        // Test to see if the process is hung up.
                        if (p.Responding)
                            // Process was responding; close the main window.
                            p.CloseMainWindow();
                        else
                            // Process was not responding; force the process to close.
                            p.Kill();
                    }

                    _log.Debug($"Patching complete for {gameId} at {clientPath}");

                }
                catch (ThreadAbortException)
                {
                    // we got killed.  let it die silently
                    _log.Debug($"Patching thread for {gameId} was aborted.");
                }
                catch (Exception ex)
                {
                    _log.Error($"Exception while patching {gameId}", ex);
                }
                finally
                {
                    _patchingDict[gameId] = false;
                }

            }

            finishedProcessingCallback?.Invoke();
        }

        private static long GetNowServingNumber(string statusUrl)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(statusUrl);
            return Convert.ToInt64(doc["Status"]["nowservingqueuenumber"].InnerText.Substring(2), 16);
        }

        private static string HttpPost(string uri, string body)
        {
            RestClient client = new RestClient(uri);
            RestRequest request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Content-Length", body.Length.ToString());

            byte[] bytes = Encoding.ASCII.GetBytes(body);
            // request.AddParameter(new Parameter())
            request.AddParameter("throwaway", body, ParameterType.RequestBody);
            var response = client.Execute(request);
            return response.Content;
        }

        public static void WaitForPatchingComplete(GameId game)
        {
            while (IsPatching(game))
                Thread.Sleep(100);
        }


        public static bool IsPatching(GameId game)
        {
            if (!_patchingDict.ContainsKey(game))
                return false;

            return _patchingDict[game];
        }
    }
}
